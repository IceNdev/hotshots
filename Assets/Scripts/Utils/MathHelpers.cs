﻿using UnityEngine;

namespace Utils
{
    public class MathHelpers
    {
        // Normalize an angle, guaranteeing it to be between -pi and pi 
        public static float NormAngle(float angle) {
            while (angle < -Mathf.PI) angle += Mathf.PI * 2f;
            while (angle > Mathf.PI) angle -= Mathf.PI * 2f;
            return angle;
        }
    }
}