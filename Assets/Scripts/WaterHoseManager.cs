﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterHoseManager : MonoBehaviour
{
    public Transform playerBody;
    public Camera fpsCamera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.localRotation = Quaternion.Euler(fpsCamera.transform.rotation.eulerAngles);
        this.transform.position = playerBody.transform.position + new Vector3(0.0f,0.0f,1.5f);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "FireMesh")
        {    
            col.gameObject.SetActive(false);
        }
    }

}
