﻿using UnityEngine;
using UnityTemplateProjects;

namespace Controllers
{
    public class HoseController : MonoBehaviour
    {
        [SerializeField] 
        private float _maxLength = 5.0f;

        [SerializeField] 
        private float _offSetY = -2f;

        [SerializeField] 
        public Hose Hose;
        
        private bool _moveStatus = true;
        private bool _initialized;
        private Vector3 _playerPosition = Vector3.zero;
        private Vector3 _position = Vector3.zero;
        
        public void Initialize() => _initialized = true;
        public void UpdatePlayerPosition(Vector3 position) => _playerPosition = position;
        
        private void FixedUpdate()
        {
            if (!_moveStatus || !_initialized || Hose == null) return;

            if (Hose.GetPositionsCount() > 0 && Hose.GetPositionsCount() <= 40)
            {
                Vector3[] positions = Hose.GetPositions();
                Vector3 lastPosition = Hose.GetLastIndex(ref positions);
                float distance = Vector3.Distance(lastPosition, _playerPosition);

                if (distance >= _maxLength)
                {
                    _position = _playerPosition;
                    _position.y += _offSetY;
                    
                    Hose.PositionCount();
                    Hose.AddPosition(Hose.GetPositionsCount() - 1, _position);
                }
            }
            if (Hose.GetPositionsCount() == 30)
            {
                NotificationManager.Instance.SendNotification("Hose", "Hose almost on max length");
                Hose.SimplifyLine();
            }
            if (Hose.GetPositionsCount() >= 40)
            {
                NotificationManager.Instance.SendNotification("Hose", "Max length reached. Hose Reset", 4f);
                Hose.Reset();
            }
        }
    }
}
