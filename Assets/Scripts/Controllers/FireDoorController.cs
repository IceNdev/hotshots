﻿using Player;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Controllers
{
    public class FireDoorController : MonoBehaviour
    {
        [Header("Rotation Values")] 
        [SerializeField]
        private float _rotationOpen = -87.0f;
        [SerializeField]
        private float _rotationClose = 0.0f;
        [SerializeField] private float _timeToRotate = 3f;
        private bool _status;
        
        public void ToggleDoorStatus()
        {
            _status = !_status;
            if (_status)
            {
                LeanTween.rotateLocal(gameObject, new Vector3(0.0f, 0.0f, _rotationOpen), _timeToRotate);
            }
            else
            {
                LeanTween.rotateLocal(gameObject, new Vector3(0.0f, 0.0f, _rotationClose), _timeToRotate);
            }
        }
    }
}
