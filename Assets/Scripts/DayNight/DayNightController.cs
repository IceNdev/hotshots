﻿using Systems.Weather;
using Systems.Weather.WeatherVisuals;
using UnityEngine;
using UnityEngine.Events;

namespace DayNight
{
    public class DayNightController : MonoBehaviour
    {
        public Transform sun;
        public Light moonLight;
        public float cylcleInMinutes = 1f;

        public AnimationCurve sunBrightness;

        public Gradient sunColor;

        [GradientUsage(true)] public Gradient skyColorDay;

        [GradientUsage(true)] public Gradient skyColorNight;

        public AnimationCurve skyIntensity;

        public AnimationCurve skyFogDensity;

        public Gradient fogColor = new Gradient();

        private enum ETimeOfDay
        {
            Night,
            Morning,
            Noon,
            Evening
        }

        private ETimeOfDay _timeOfDay = ETimeOfDay.Night;
        private ETimeOfDay _todMessageCheck = ETimeOfDay.Night;
    
        public float starsSpeed = 0.2f;
        public Vector2 cloudsSpeed = new Vector2(0.1f, 0f);
    
        public UnityEvent onMidnight;
        public UnityEvent onMorning;
        public UnityEvent onNoon;
        public UnityEvent onEvening;
    
        public float DecimalTime
        {
            get => _decimalTime;
            private set => _decimalTime = value;
        } 

        private Light _sunLight;
        private float _sunAngle;
        private float _decimalTime;
        private WeatherVisual _weatherVisual;
    
        void Start()
        {
            sun.rotation = Quaternion.Euler(0f, -90.0f, 0f);
            _sunLight = sun.GetComponent<Light>();
        
        }

        // Update is called once per frame
        void Update()
        {
            UpdateWeatherVisual();
        
            UpdateSunAngle();
            RotateSun();
            SetSunBrightness();
            SetSunColor();
            SetSkyColor();
            MoveStars();
            MoveClouds();
            SetFogColor();
            UpdateDecimalTime();
            UpdateETimeOfDay();
        }

        private float _transitionTime = 3.0f;
        private void UpdateWeatherVisual()
        {
            // TODO: Make a smooth transition over time
            _weatherVisual = WeatherManager.Instance.GetWeatherCondition().Visual;
            sunBrightness = _weatherVisual.sunBrightness;
            sunColor = _weatherVisual.sunColor;
            //StartCoroutine(SmoothColorTransition());
            skyColorDay = _weatherVisual.skyColorDay;
            skyColorNight = _weatherVisual.skyColorNight;
            skyIntensity = _weatherVisual.skyIntensity;
            skyFogDensity = _weatherVisual.skyFogDensity;
            fogColor = _weatherVisual.fogColor;
        
            //SmoothTransition();
        }

        private void SmoothTransition()
        {
            float sunBrightnessNew = sunBrightness.Evaluate(_sunAngle);
            float lerpValue = Mathf.Lerp(_sunLight.intensity, sunBrightness.Evaluate(_sunAngle), Time.deltaTime * 0.001f);
            Debug.Log($"Lerp Value: {lerpValue} | Sun: {_sunLight.intensity} | SunNew: {sunBrightnessNew}");
            _sunLight.intensity += Mathf.Lerp(_sunLight.intensity, sunBrightness.Evaluate(_sunAngle), Time.deltaTime * 0.001f);
        }
        /*
    private IEnumerator SmoothColorTransition()
    {
        float t = 0f;
        while (t < _fadeTime)
        {
            t += Time.unscaledDeltaTime;
            float time = t / _fadeTime;
            float evaluate = 0;
            skyColorDay.colorKeys[1].color = Color.Lerp(skyColorDay.Evaluate(evaluate), _weatherVisual.skyColorDay.Evaluate(evaluate), time);
            skyColorNight.colorKeys[1].color = Color.Lerp(skyColorNight.Evaluate(evaluate), _weatherVisual.skyColorNight.Evaluate(evaluate), time);
            yield return null;
        }
    }*/

        private void UpdateSunAngle()
        {
            // We use signedAngle so we can distinct morning and evening, we get a result between -180 to 180 and 
            // dividing by 360 it goes from -0.5 to 0.5, By adding 0.5f we get a range from 0 to 1
            _sunAngle = Vector3.SignedAngle(Vector3.down, sun.forward, sun.right);
            _sunAngle = (_sunAngle / 360.0f) + 0.5f;
        }
    
        private void RotateSun()
        {
            // Rotate on the x-axis by one degree per second
            sun.Rotate(Vector3.right * Time.deltaTime * 6 / cylcleInMinutes);
        }

        void SetSunBrightness()
        {
            // Adjust the sun brightness according to the sun rotation
            _sunLight.intensity = sunBrightness.Evaluate(_sunAngle);
        }

        void SetSunColor()
        {
            _sunLight.color = sunColor.Evaluate(_sunAngle);
        }

        void SetSkyColor()
        {
            if (_sunAngle >= 0.25f && _sunAngle < 0.75f)
                RenderSettings.skybox.SetColor("_SkyColor2", skyColorDay.Evaluate(_sunAngle * 2.0f - 0.5f));
            else if (_sunAngle > 0.75f)
                RenderSettings.skybox.SetColor("_SkyColorNight2", skyColorNight.Evaluate(_sunAngle * 2.0f - 1.5f));
            else
                RenderSettings.skybox.SetColor("_SkyColorNight2", skyColorNight.Evaluate(_sunAngle * 2.0f + 0.5f));
        
            RenderSettings.skybox.SetFloat("_SkyIntensity", skyIntensity.Evaluate(_sunAngle));
        }

        void MoveStars()
        {
            RenderSettings.skybox.SetVector("_StarsOffset", new Vector2(_sunAngle * starsSpeed, 0));
        }

        void MoveClouds()
        {
            Vector2 actualCloudsOffset = (Vector2) RenderSettings.skybox.GetVector("_CloudsOffset");
            RenderSettings.skybox.SetVector("_CloudsOffset", actualCloudsOffset + Time.deltaTime * cloudsSpeed);
        }

        void SetFogColor()
        {
            RenderSettings.fogColor = fogColor.Evaluate(_sunAngle);
            RenderSettings.fogDensity = skyFogDensity.Evaluate(_sunAngle);
        }

        void UpdateDecimalTime()
        {
            // 0.25 because the day starts at morning. Time.time times 6 because 360 degrees in a full rotation.
            // Modulo(%) 1 makes the value go from 0 to 1 repeatedly.
            _decimalTime = (0.25f + Time.time * 6 / cylcleInMinutes / 360) % 1;
        
            //Debug.Log(_decimalTime); 
        }
    
        private void UpdateETimeOfDay()
        {
            if (_decimalTime > 0.25f && _decimalTime < 0.5f)
                _timeOfDay = ETimeOfDay.Morning;
            else if (_decimalTime > 0.5f && _decimalTime < 0.75f)
                _timeOfDay = ETimeOfDay.Noon;
            else if (_decimalTime > 0.75f)
                _timeOfDay = ETimeOfDay.Evening;
            else
                _timeOfDay = ETimeOfDay.Night;

            // Check if the timeOfDay has changed. If so, invoke the event.
            // With this we make sure the event it's only invoked once.
            if (_todMessageCheck != _timeOfDay)
            {
                InvokeTimeOfDayEvent();
                _todMessageCheck = _timeOfDay;
            }
        }

        private void InvokeTimeOfDayEvent()
        {
            switch (_timeOfDay)
            {
                case ETimeOfDay.Night:
                    onMidnight?.Invoke();
                    Debug.Log("OnMidnight");
                    break;
                case ETimeOfDay.Morning:
                    WeatherManager.Instance.ChangeWeather();
                    SunToMoon(true);
                    onMorning?.Invoke();
                    Debug.Log("OnMorning");
                    break;
                case ETimeOfDay.Noon:
                    onNoon?.Invoke();
                    Debug.Log("OnNoon");
                    break;
                case ETimeOfDay.Evening:
                    SunToMoon(false);
                    onEvening?.Invoke();
                    Debug.Log("OnEvening");
                    break;
            }
        }

        private void SunToMoon(bool status)
        {
            _sunLight.enabled = status;
            moonLight.enabled = !status;
        }
    }
}
