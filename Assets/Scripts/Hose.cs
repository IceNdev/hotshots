﻿using Systems.Quartel;
using UnityEngine;

public class Hose : MonoBehaviour
{
    private LineRenderer _lineRenderer;
    private MeshCollider _meshCollider;
    public WaterTank WaterTank1 { get; set; }
    public WaterTank WaterTank2 { get; set; }

    public void SimplifyLine()
    {
        _lineRenderer.Simplify(2f);
    }
    
    public void AddHoseConnectors(bool first, Vector3 position)
    {
        _lineRenderer.positionCount++;
            
        int index = 0;
        if (!first)
            index = _lineRenderer.positionCount - 1;
                
        _lineRenderer.SetPosition(index, position);
        UpdateMeshCollider();
    }

    public void AddPosition(int index, Vector3 position)
    {
        _lineRenderer.SetPosition(index, position);
        _lineRenderer.Simplify(0.1f);
        UpdateMeshCollider();
    }

    public void PositionCount()
    {
        _lineRenderer.positionCount++;
    }

    public void Reset()
    {
        _lineRenderer.positionCount = 0;
        WaterTank1 = null;
        WaterTank2 = null;
    }

    public bool Active() => _lineRenderer.positionCount > 0;

    public Vector3[] GetPositions()
    {
        Vector3[] positions = new Vector3[_lineRenderer.positionCount];
        _lineRenderer.GetPositions(positions);

        return positions;
    }
    
    public Vector3 GetLastIndex(ref Vector3[] positions)
    {
        return positions[positions.Length - 1];
    }

    public int GetPositionsCount() => _lineRenderer.positionCount;
    
    private void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _meshCollider = GetComponent<MeshCollider>();
        _meshCollider.convex    = true;
        _meshCollider.isTrigger = true;
        
        UpdateMeshCollider();
    }

    private void UpdateMeshCollider()
    {
        Mesh mesh = new Mesh();
        _lineRenderer.BakeMesh(mesh, true);
        _meshCollider.sharedMesh = mesh;
    }
}