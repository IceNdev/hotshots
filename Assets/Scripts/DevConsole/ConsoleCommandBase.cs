﻿using System;

namespace DevConsole
{
    public class ConsoleCommandBase
    {
        private string _commandId;
        private string _commandDescription;
        private string _commandFormat;
        private string _commandOutput;

        public ConsoleCommandBase(string commandId, string commandDescription, string commandFormat, string commandOutput)
        {
            _commandId = commandId;
            _commandDescription = commandDescription;
            _commandFormat = commandFormat;
            _commandOutput = commandOutput;
        }

        public string CommandId => _commandId;
        public string CommandDescription => _commandDescription;
        public string CommandFormat => _commandFormat;
        public string CommandOutput => _commandOutput;
    }

    public class ConsoleCommand : ConsoleCommandBase
    {
        private Action _command;
        
        public ConsoleCommand(string id, string description, string format, Action command, string output = null) : base(id, description,
            format, output)
        {
            _command = command;
        }

        public void Invoke()
        {
            _command.Invoke();
        } 
    }
    public class ConsoleCommand<T1> : ConsoleCommandBase
    {
        private Action<T1> _command;
        
        public ConsoleCommand(string id, string description, string format, Action<T1> command, string output = null) : base(id, description,
            format, output)
        {
            _command = command;
        }

        public void Invoke(T1 value)
        {
            _command.Invoke(value);
        } 
    }
}