﻿using System;
using System.Collections.Generic;
using Systems;
using Systems.Quartel;
using Player;
using SaveData.SaveDataManagers;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityTemplateProjects;
using Vehicle;

namespace DevConsole
{
    public class DevConsole : MonoBehaviour
    {
        private PlayerInActions _controls;
        private bool _showConsole;
        private bool _showHelp;
        private bool _showOutput;
        private string _textOutput;
        private string _input = "<type here>";
        private const string TEXT_FIELD_NAME = "ConsoleTextField";
        private Vector2 _scroll;
        
        // Commands
        public static ConsoleCommand<int> ADD_MONEY;
        public static ConsoleCommand MONEY;
        public static ConsoleCommand HELP;
        public static ConsoleCommand WATERTANK_INFO;
        public static ConsoleCommand SEND_NOTIFICATION;
        public static ConsoleCommand<int> ADD_VEHICLE_SLOT;
        public static ConsoleCommand<int> REMOVE_VEHICLE_SLOT;
        public static ConsoleCommand AI_SEARCH_VEHICLE;
        public static ConsoleCommand SAVE_VEHICLES;

        public List<object> _CommandsList;

        private void Awake()
        {
            ADD_MONEY = new ConsoleCommand<int>("addMoney", "Adds money", "addMoney <value>", (value) =>
            {
                EconomyManager.Instance.AddMoney(value);
                _textOutput = $"Added {value} to the account";
                _showOutput = true;
                _showHelp = false;
            });
            
            MONEY = new ConsoleCommand("money", "Show the balance", "money", () =>
            {
                _showOutput = true;
                _showHelp = false;
                _textOutput = $"Money: {EconomyManager.Instance.Money.ToString()}";
            });
            
            HELP = new ConsoleCommand("help", "Shows a list of commands", "help", () =>
            {
                _showOutput = false;
                _showHelp = true;
            });
            
            WATERTANK_INFO = new ConsoleCommand("watertankInfo", "Show's the watertank information", "watertankInfo", () =>
            {
                _showOutput = true;
                _showHelp = false;
                
                _textOutput = GetFrontTankInfo();
            });
            
            SEND_NOTIFICATION = new ConsoleCommand("sendNotification", "Send notification", "sendNotification",
                () =>
            {
                NotificationManager.Instance.SendNotification("Test", "asdasd");
            });
            
            ADD_VEHICLE_SLOT = new ConsoleCommand<int>("addVehicleSlot", "Add vehicle slot", "addVehicleSlot <value>", (value) =>
            {
                GameObject truck = GameObject.Find("VLCI");
                truck.GetComponent<VehicleInfo>().AddFirefighter((Convert.ToBoolean(value)));
            });
            
            REMOVE_VEHICLE_SLOT = new ConsoleCommand<int>("removeVehicleSlot", "Remove vehicle slot", "removeVehicleSlot <value>", (value) =>
            {
                GameObject truck = GameObject.Find("VLCI");
                truck.GetComponent<VehicleInfo>().RemoveFirefighter((Convert.ToBoolean(value)));
            });
            
            AI_SEARCH_VEHICLE = new ConsoleCommand("aiSearchVehicle", "Search a vehicle to get in", "aiSeachVehicle", () =>
            {
                AIManager.Instance.EnterVehicle();
            });
            
            SAVE_VEHICLES = new ConsoleCommand("saveVehicles", "saves vehicles' info", "saveVehicles", () =>
            {
                VehicleDataManager.Instance.OnSave();
            });
            
            _CommandsList = new List<object>()
            {
                ADD_MONEY,
                MONEY,
                WATERTANK_INFO,
                HELP,
                SEND_NOTIFICATION,
                ADD_VEHICLE_SLOT,
                REMOVE_VEHICLE_SLOT,
                AI_SEARCH_VEHICLE,
                SAVE_VEHICLES
            };
            
            _controls = PlayerInputs.Instance.PlayerControls;
            _controls.UI.DebugConsole.performed += DebugConsoleOnperformed;
            _controls.UI.DebugConsoleReturn.performed += DebugConsoleReturnOnperformed;
        }

        private void OnEnable()
        {
            _controls.UI.DebugConsole.Enable();
            _controls.UI.DebugConsoleReturn.Enable();
        }

        private void OnDisable()
        {
            _controls.UI.DebugConsole.Disable();
            _controls.UI.DebugConsoleReturn.Disable();
        }

        private void OnGUI()
        {
            if (!_showConsole) return;
            
            float y = 0.0f;

            if (_showHelp)
            {
                GUI.Box(new Rect(0, y, Screen.width, 100f), "");
                Rect viewPort = new Rect(0.0f, 0.0f, Screen.width - 30.0f, 20.0f * _CommandsList.Count);
                
                _scroll = GUI.BeginScrollView(new Rect(0.0f, y + 5f, Screen.width, 90f), _scroll, viewPort);

                for (int i = 0; i < _CommandsList.Count; i++)
                {
                    ConsoleCommandBase command = _CommandsList[i] as ConsoleCommandBase;
                    string text = $"{command.CommandFormat} -> {command.CommandDescription}";
                    Rect labelRect = new Rect(5f, 20f * i, viewPort.width - 100f, 20f);
                    GUI.Label(labelRect, text);
                }
                
                GUI.EndScrollView();
                y += 100f;
            }
            if (_showOutput)
            {
                GUI.Box(new Rect(0f, y, Screen.width, 30f), "");
                
                Rect labelRect = new Rect(10f, y + 5f, Screen.width - 20f, 20f);
                
                GUI.Label(labelRect, _textOutput);
                y += 30;
            }
            
            GUI.Box(new Rect(0, y, Screen.width, 30), "");
            GUI.backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
            
            GUI.SetNextControlName(TEXT_FIELD_NAME);
            _input = GUI.TextField(new Rect(10.0f, y + 5.0f, Screen.width - 20.0f, 20.0f), _input);
            GUI.FocusControl(TEXT_FIELD_NAME);
        }

        private void HandleInput()
        {
            string[] properties = _input.Split(' ');
            for (int i = 0; i < _CommandsList.Count; i++)
            {
                ConsoleCommandBase commandBase = _CommandsList[i] as ConsoleCommandBase;
                if (_input.Contains(commandBase.CommandId))
                {
                    if (_CommandsList[i] as ConsoleCommand != null)
                    {
                        (_CommandsList[i] as ConsoleCommand)?.Invoke();
                    }
                    else if (_CommandsList[i] as ConsoleCommand<int> != null)
                    {
                        (_CommandsList[i] as ConsoleCommand<int>)?.Invoke(int.Parse(properties[1]));
                    }
                }
            }
        }

        private void DebugConsoleOnperformed(InputAction.CallbackContext obj)
        {
            _showConsole = !_showConsole;
            if (_showConsole)
            {
                _controls.Player.Disable();
                _controls.Vehicle.Disable();
            }
            else
            {
                _showHelp = false;
                _showOutput = false;
                _controls.Player.Enable();
                _controls.Vehicle.Enable();
            }
        }
        
        private void DebugConsoleReturnOnperformed(InputAction.CallbackContext obj)
        {
            if (_showConsole)
            {
                HandleInput();
                _input = String.Empty;
            }
        }
        
        // It's not working because the script devconsole is in the UI
        // TODO: FIX Physics.OverlapSphere
        private string GetFrontTankInfo()
        {
            string output = String.Empty;
            Collider[] colliders = Physics.OverlapSphere(transform.position, 10f, 0);

            foreach (Collider collider in colliders)
            {
                if (collider.CompareTag("WaterTank") || collider.CompareTag("Truck"))
                {
                    WaterTank waterTank = collider.gameObject.GetComponent<WaterTank>();
                    if (waterTank != null)
                    {
                        output = $"WaterTank Info: Fixed: Water Loaded: {waterTank.WaterLoaded}, Free Capacity: {waterTank.FreeCapacity()}";
                        if (collider.CompareTag("Truck"))
                            output += " (Truck) ";
                    }
                }
            }

            return output;
        }
    }
}
