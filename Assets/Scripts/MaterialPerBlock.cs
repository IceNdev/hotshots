﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialPerBlock : MonoBehaviour
{
    public Vector2 Tilling;
    public int MaterialIndex = 0;
    
    private MeshRenderer _renderer;
    private MaterialPropertyBlock _propertyBlock;
    
    private void OnValidate()
    {
        Tilling = new Vector2(transform.localScale.z, transform.localScale.x);
        int propertyID = Shader.PropertyToID("_MainTex_ST");
        
        _renderer = GetComponent<MeshRenderer>();
        _propertyBlock = new MaterialPropertyBlock();
        
        _renderer.GetPropertyBlock(_propertyBlock, MaterialIndex);
        _propertyBlock.SetVector(propertyID, Tilling);
        //_propertyBlock.SetColor(1, Color.black);
        _renderer.SetPropertyBlock(_propertyBlock, MaterialIndex);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        int propertyID = Shader.PropertyToID("_MainTex_ST");
        
        _renderer = GetComponent<MeshRenderer>();
        _propertyBlock = new MaterialPropertyBlock();
        
        _renderer.GetPropertyBlock(_propertyBlock, 1);
        _propertyBlock.SetVector(propertyID, Tilling);
        //_propertyBlock.SetColor(1, Color.black);
        _renderer.SetPropertyBlock(_propertyBlock, 1);
    }
 
}
