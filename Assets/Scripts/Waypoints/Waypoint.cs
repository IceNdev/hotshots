﻿// ---------------------------------------
// Credits to "Game Dev Guide" on youtube
// ---------------------------------------

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    public Waypoint next;
    public Waypoint previous;

    public float width = 1f;
    public List<Waypoint> branches;

    [Range(0f, 1f)] public float branchRatio = 0.5f;

    [SerializeField] public TrafficLightController _trafficLightController = null;

    public Vector3 GetPosition()
    {
        Vector3 minBound = transform.position + transform.right * width / 2f;
        Vector3 maxBound = transform.position - transform.right * width / 2f;

        return Vector3.Lerp(minBound, maxBound, Random.Range(0f, 1f));
        return transform.position;
    }

}
