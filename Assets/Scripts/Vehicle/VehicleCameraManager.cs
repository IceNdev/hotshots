﻿using System;
using Cinemachine;
using Player;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Vehicle
{
    public class VehicleCameraManager : MonoBehaviour
    {
        [Range(0f, 10f)] public float LookSpeed = 1f;
        public bool InvertY = false;
        public CinemachineFreeLook cineMachine;
        
        private PlayerInActions _controls = null;
        private InputAction _mouseInput;
        private bool _cameraRecenter = true;

        private void Awake()
        {
            _controls = PlayerInputs.Instance.PlayerControls;
            _mouseInput = _controls.Vehicle.Look;
            _controls.Vehicle.CameraRecenter.performed += CameraRecenterOnperformed;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void CameraRecenterOnperformed(InputAction.CallbackContext obj)
        {
            _cameraRecenter = !_cameraRecenter;
            cineMachine.m_RecenterToTargetHeading.m_enabled = _cameraRecenter;
        }

        private void FixedUpdate()
        {
            if (_cameraRecenter) return;
            
            // Normalize the vector to have an uniform vector in whichever form it came from (I.E Gamepad, mouse, etc)
            Vector2 lookMovement = _mouseInput.ReadValue<Vector2>().normalized;
            lookMovement.y = InvertY ? -lookMovement.y : lookMovement.y;

            // This is because X axis is only contains between -180 and 180 instead of 0 and 1 like the Y axis
            lookMovement.x = lookMovement.x * 180f; 

            // Ajust axis values using look speed and Time.deltaTime so the look doesn't go faster if there is more FPS
            cineMachine.m_XAxis.Value += lookMovement.x * LookSpeed * Time.deltaTime;
            cineMachine.m_YAxis.Value += lookMovement.y * LookSpeed * Time.deltaTime;
        }

        public void UpdateChinemachine(Transform vehicleTransform)
        {
            cineMachine.Follow = vehicleTransform;
            cineMachine.LookAt = vehicleTransform;
        }

    }
}