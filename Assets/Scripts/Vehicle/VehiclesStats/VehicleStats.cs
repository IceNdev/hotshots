﻿using UnityEngine;

namespace Vehicle.VehiclesStats
{
    [CreateAssetMenu(fileName = "VehicleStats", menuName = "Vehicles", order = 0)]
    public class VehicleStats : ScriptableObject
    {
        public int WaterCapacity;
        public int WaterLoaded { get; private set; }

        public void AddWater(int value) => WaterLoaded += value;
    }
}