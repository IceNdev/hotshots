﻿using System.Collections;
using UnityEngine;

namespace Vehicle
{
    public class VehicleLightSystem : MonoBehaviour
    {
        [Header("Lights")]
        [SerializeField]
        private Light[] _frontLights;

        private Material _frontLightsMat;
        
        private Material _backLightsMat;
        
        private Material _brakeLightsMat;
        
        private Material[] _hazardLightsMats;

        [SerializeField] 
        private Light[] _emergencyLights;
        
        private Material[] _emergencyLightsMats;

        [SerializeField]
        private Renderer[] _renderers;
        
        [Header("Sounds")]
        [SerializeField] 
        private AudioSource _sirenSound;
        
        private bool _isHazardLightsOn, _isHazardBlinking;
        private bool _isEmergencyLightsOn, _isEmergencyBlinking;
        
        public bool GetBrakeLightsStatus { get; private set; }

        private void Start()
        {
            _emergencyLightsMats = new Material[_renderers.Length];
            _hazardLightsMats    = new Material[_renderers.Length];
            for (int i = 0; i < _renderers.Length; i++)
            {
                Renderer renderer = _renderers[i];
                foreach (Material material in renderer.materials)
                {
                    string materialName = material.name;
                    if (materialName.Contains("Head"))
                        _frontLightsMat = material;
                    else if (materialName.Contains("Back"))
                        _backLightsMat = material;
                    else if (materialName.Contains("Brake"))
                        _brakeLightsMat = material;
                    else if (materialName.Contains("Hazard"))
                        _hazardLightsMats[i] = material;
                    else if (materialName.Contains("Siren"))
                        _emergencyLightsMats[i] = material;
                }
            }
        }
        
        private void Update()
        {
            if (_isHazardLightsOn && _isHazardBlinking == false)
                StartCoroutine(HazardLightsAnim());
            
            if (_isEmergencyLightsOn && _isEmergencyBlinking == false)
                StartCoroutine(EmergencyLightsAnim());
            
            // Make sure all the emergency lights are off
            if (!_isEmergencyLightsOn)
            {
                for (int i = 0; i < _emergencyLights.Length; i++)
                    _emergencyLights[i].enabled = false;

                foreach (Material emergencyLightsMat in _emergencyLightsMats)
                {
                    emergencyLightsMat.DisableKeyword("_EMISSION");
                }
                //_emergencyLightsMat.DisableKeyword("_EMISSION");
            }
        }
        
        // ----------------------------
        //      Toggle Light Actions
        // ----------------------------
        
        public void ToggleHazardLights() => _isHazardLightsOn = !_isHazardLightsOn;

        public void ToggleEmergencyLights()
        {
            _isEmergencyLightsOn = !_isEmergencyLightsOn;
            if (_isEmergencyLightsOn)
                _sirenSound.Play();
            else
                _sirenSound.Stop();
        } 
        public void ToggleHeadLights()
        {
            bool status = !_frontLights[0].enabled;
            ToggleFrontLights(status);
            ToggleBackLights(status);
        }

        private void ToggleFrontLights(bool status)
        {
            _frontLights[0].enabled = status;
            _frontLights[1].enabled = status;

            if (status)
            {
                _frontLightsMat.EnableKeyword("_EMISSION");
                _frontLightsMat.EnableKeyword("_EMISSION");
            }
            else
            {
                _frontLightsMat.DisableKeyword("_EMISSION");
                _frontLightsMat.DisableKeyword("_EMISSION");
            }
        }
        private void ToggleBackLights(bool status)
        {
            //_backLights[0].enabled = status;
            //_backLights[1].enabled = status;
            
            if (status)
            {
                _backLightsMat.EnableKeyword("_EMISSION");
                _backLightsMat.EnableKeyword("_EMISSION");
            }
            else
            {
                _backLightsMat.DisableKeyword("_EMISSION");
                _backLightsMat.DisableKeyword("_EMISSION");
            }
        }
        public void ToggleBrakeLights(bool status)
        {
            //_brakeLights[0].enabled = true;
            //_brakeLights[1].enabled = true;
            
            GetBrakeLightsStatus = status;
            if (status)
            {
                _brakeLightsMat.EnableKeyword("_EMISSION");
                _brakeLightsMat.EnableKeyword("_EMISSION");
            }
            else
            {
                _brakeLightsMat.DisableKeyword("_EMISSION");
                _brakeLightsMat.DisableKeyword("_EMISSION");
            }
        }

        // ----------------------------
        //        Lights Animations
        // ----------------------------
        
        private IEnumerator HazardLightsAnim()
        {
            _isHazardBlinking = true;
            
            
            //_hazardLightsMat.EnableKeyword("_EMISSION");
            foreach (Material hazardLightsMat in _hazardLightsMats)
            {
                hazardLightsMat.EnableKeyword("_EMISSION");
            }

            /*for (int i = 0; i < _hazardLights.Length; i++)
                _hazardLights[i].enabled = true;*/
                            
            yield return new WaitForSeconds(1f);
            
            //_hazardLightsMat.DisableKeyword("_EMISSION");
            foreach (Material hazardLightsMat in _hazardLightsMats)
            {
                hazardLightsMat.DisableKeyword("_EMISSION");
            }
            
            /*for (int i = 0; i < _hazardLights.Length; i++)
                _hazardLights[i].enabled = false;*/
            
            yield return new WaitForSeconds(1.0f);
            _isHazardBlinking = false;
        }
        
        private IEnumerator EmergencyLightsAnim()
        {
            _isEmergencyBlinking = true;

            /*
            _emergencyLightsMat.EnableKeyword("_EMISSION");
            _emergencyLights[0].enabled = true;
            _emergencyLightsMat.DisableKeyword("_EMISSION");
            _emergencyLights[1].enabled = false;
            
            yield return new WaitForSeconds(0.1f);
            
            _emergencyLightsMat.DisableKeyword("_EMISSION");
            _emergencyLights[0].enabled = false;
            _emergencyLightsMat.EnableKeyword("_EMISSION");
            _emergencyLights[1].enabled = true;*/
            
            foreach (Material emergencyLightsMat in _emergencyLightsMats)
            {
                emergencyLightsMat.EnableKeyword("_EMISSION");
                _emergencyLights[0].enabled = true;
                emergencyLightsMat.DisableKeyword("_EMISSION");
                _emergencyLights[1].enabled = false;
            
                yield return new WaitForSeconds(0.1f);
            
                emergencyLightsMat.DisableKeyword("_EMISSION");
                _emergencyLights[0].enabled = false;
                emergencyLightsMat.EnableKeyword("_EMISSION");
                _emergencyLights[1].enabled = true;
            }
            
            yield return new WaitForSeconds(0.05f);
            _isEmergencyBlinking = false;
        }


    }   
}
