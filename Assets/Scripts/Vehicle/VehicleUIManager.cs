﻿using TMPro;
using UnityEngine;

namespace Vehicle
{
    public class VehicleUIManager : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _textVelocimeter;
        
        public void SetSpeedText(string text)
        {
            _textVelocimeter.SetText(text);
        }
        
    }
}