﻿using System;
using Systems.GPS;
using Systems.Quartel;
using SaveData;
using UnityEngine;

namespace Vehicle
{
    public class VehicleInfo : MonoBehaviour
    {
        private string _guid;
        
        [SerializeField] 
        private string _name;

        [SerializeField] public EPrefabType PrefabType;

        [SerializeField] private EVehicleSize _sizeType;
        
        [SerializeField]
        private Transform _exitPosition;

        [SerializeField] 
        private Transform _hoseConnector;

        [SerializeField] 
        private GameObject[] _fireFightersSlots;

        [SerializeField] 
        private GameObject _playerSlot;

        [SerializeField]
        private GPSPathFinder _gps;

        private byte _slotsAvailable;
        private bool _driverSeatOccupied;
        
        public byte Slots => (byte)(_fireFightersSlots.Length - 1);
        public Vector3 GetExitPosition() => _exitPosition.position;

        public Transform Transform => transform;

        public string GetId() => _guid;
        public string GetName() => _name;

        private void Start()
        {
            _guid = System.Guid.NewGuid().ToString();
            _slotsAvailable = Convert.ToByte(Slots + 1);
        }
        
        public void AddFirefighter(bool isDriver)
        {
            if (isDriver)
            {
                _fireFightersSlots[0].SetActive(true);
            }
            else if (_slotsAvailable > 0)
                _fireFightersSlots[_slotsAvailable--].SetActive(true);
        }
        
        public void RemoveFirefighter(bool isDriver)
        {
            if(isDriver)
                _fireFightersSlots[0].SetActive(false);
            else if (_slotsAvailable < Slots)
                _fireFightersSlots[++_slotsAvailable].SetActive(false);
        }
        
        public void EnterDriverSeat(bool isPlayer)
        {
            if (isPlayer)
                _playerSlot.SetActive(true);
            else
                _fireFightersSlots[0].SetActive(true);
            
            _driverSeatOccupied = true;
        }
        
        public void EnterPassengerSeat()
        {
            if (_slotsAvailable > 1)
            {
                _fireFightersSlots[--_slotsAvailable].SetActive(true);
                Debug.Log($"Index: {_slotsAvailable}");
            }
        }
        
        public void GetOutDriverSeat(bool isPlayer)
        {
            if (isPlayer)
                _playerSlot.SetActive(false);
            else
                _fireFightersSlots[0].SetActive(false);
            
            _driverSeatOccupied = false;
        }
        
        public void GetOutPassengerSeat()
        {
            if (_slotsAvailable < Convert.ToByte(Slots + 1))
                _fireFightersSlots[_slotsAvailable++].SetActive(false);
        }

        public bool IsFull() => _slotsAvailable <= 1;

        public bool GetDriverSeatStatus() => _driverSeatOccupied;
        public EVehicleSize SizeType => _sizeType;
        public void SetGPSStatus(bool status) => _gps.SetActive(status);
    }
}