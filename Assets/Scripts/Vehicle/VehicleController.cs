﻿/*
   Class from Polarith AI Free | Movement, Steering
   http://polarith.com/ai/ 
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Systems.GPS;
using Systems.Quartel;
using Player;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.InputSystem;
using Vehicle;

namespace Vehicle
{
    [RequireComponent(typeof(Rigidbody), typeof(VehicleLightSystem))]
    public class VehicleController : MonoBehaviour
    {
        [Header("Vehicle Settings")]
        public float CarBase = 3f;
        public float CarWidth = 2f;
        
        public List<WheelCollider> wheelColliders = new List<WheelCollider>();    
        
        public List<WheelCollider> motorWheels = new List<WheelCollider>();
        
        public List<WheelCollider> brakeWheels = new List<WheelCollider>();
        
        public List<GameObject> wheelMeshes = new List<GameObject>();
        
        public List<WheelCollider> steeringWheels = new List<WheelCollider>();

        [Range(0.0f, 1.0f)]
        public float steerHelper;
        
        public float slipLimit;
        
        public float brakeTorque;
        
        [Range(0.0f, 1.0f)]
        public float tractionControl;
        
        public float fullTorqueOverAllWheels;
        
        public float reverseTorque;
        
        public float downForce = 100f;
        
        public float maximumSteerAngle = 25.0f;
        
        public float topSpeed = 90f;
        
        public float minimumSpeed = -15f;

        public Transform centerOfMass;
        
        private float _currentTorque;
        private Rigidbody _body;
        private float _oldRotation;
        private bool _isEngineOn;
        private bool _isHandBrakeOn;
        private WaterTank _waterTank;
        
        public bool IsEngineOn => _isEngineOn;
        
        // Sound
        [Header("Sounds")]
        [SerializeField]
        private AnimationCurve _pitchCurve;
        [SerializeField]
        private AudioSource _vehicleSound;
        
        // Lights
        private VehicleLightSystem _vehicleLightSystem;
        
        // Input
        private PlayerInActions _controls;
        public bool IsAIControlling { get; set; }
        
        // UI 
        [SerializeField] 
        private string _vehicleUIName;

        [SerializeField] 
        public string _waterPumpUIName;
        
        private VehicleUIManager _vehicleUIManager;
        
        private void Awake()
        {
            _controls = PlayerInputs.Instance.PlayerControls;
            _controls.Vehicle.HandBreak.performed += HandBreakOnperformed;
        }

        private void OnEnable()
        {
            _controls.Vehicle.Enable();
        }

        private void OnDisable()
        {
            _controls.Vehicle.Disable();
        }

        // Start is called before the first frame update
        void Start()
        {
            _body               = GetComponent<Rigidbody>();
            _vehicleLightSystem = GetComponent<VehicleLightSystem>();
            _waterTank          = GetComponent<WaterTank>();
            _currentTorque      = fullTorqueOverAllWheels - (tractionControl * fullTorqueOverAllWheels);
            wheelColliders[0].ConfigureVehicleSubsteps(5.0f, 12, 15);
            _body.centerOfMass  = centerOfMass.localPosition;
            _isHandBrakeOn      = true;
            // Finds the Vehicle UI
            UpdateVehicleUI();
        }
        
        // Update is called once per frame
        void FixedUpdate()
        {
            if (IsAIControlling) return;
            Vector2 movementInput = _controls.Vehicle.Move.ReadValue<Vector2>();
            Vector2 steerInput = _controls.Vehicle.Steer.ReadValue<Vector2>();
           
            float steering = Mathf.Clamp(steerInput[0], -1.0f, 1.0f);

            float acceleration = 0.0f;
            float brake = 0.0f;
            if (_isEngineOn)
            {
                // Make sure the hose is not connected
                if (_waterTank.Hose == null || (_waterTank.Hose != null && !_waterTank.Hose.Active()))
                {
                    acceleration = Mathf.Clamp(movementInput[1], 0.0f, 1.0f);
                    brake = -1.0f * Mathf.Clamp(movementInput[1], -1.0f, 0.0f);
                }
                else
                {
                    // TODO: Displays a notification, because the hose is connected
                }
            }

            Move(steering, acceleration, brake);
            if(_vehicleUIManager != null)
                _vehicleUIManager.SetSpeedText($"{Math.Round(transform.InverseTransformDirection(_body.velocity).z * 3.6f, 0).ToString()} Km/h");
        }

        private void UpdateVehicleUI()
        {
            if (String.IsNullOrEmpty(_waterPumpUIName) || String.IsNullOrEmpty(_vehicleUIName))
            {
                Debug.LogWarning("You need to set the UI objects name");
                return;
            }
            GameObject vehicleUIObject = GameObject.Find(_vehicleUIName);
            _vehicleUIManager = vehicleUIObject.GetComponent<VehicleUIManager>();
            TextMeshProUGUI pumpModeText = vehicleUIObject.GetComponentsInChildren<TextMeshProUGUI>().ToList().Find(x => x.name == _waterPumpUIName);
            _waterTank.UpdateUI(vehicleUIObject.GetComponentInChildren(typeof(ProgressBar)) as ProgressBar, pumpModeText);
        }

        public void Move(float steering, float acceleration, float brake)
        {
            DriveCar(acceleration, brake);
            ApplySteeringAngle(steering);
            ApplySteerHelper();
            CapSpeed();
            ApplyDownforce();
            ApplyTractionControl();
            UpdateWheelTransform();
        }

        // Update Wheel Mesh Transform
        private void UpdateWheelTransform()
        {
            Quaternion rotation;
            Vector3 position;
            for (int i = 0; i < wheelColliders.Count; i++)
            {
                wheelColliders[i].GetWorldPose(out position, out rotation);
                wheelMeshes[i].transform.position = position;
                wheelMeshes[i].transform.rotation = rotation;
            }
        }

        private void DriveCar(float acceleration, float brake)
        {
            float thrustTorque = 0.0f;

            if (motorWheels.Count > 0)
                thrustTorque = (acceleration * 60) * (_currentTorque / motorWheels.Count) * Time.deltaTime;

            foreach (WheelCollider motorWheel in motorWheels)
                motorWheel.motorTorque = thrustTorque;
            
            // Reset brake torque, otherwise this could lead to problems and the car won't start again.
            foreach (WheelCollider brakeWheel in brakeWheels)
                brakeWheel.brakeTorque = 0.0f;
            
            // Use the brake torque if the vehicle is moving to fast. Else, use the motor for braking.
            if (_body.velocity.magnitude > 1 && Vector3.Angle(transform.forward, _body.velocity) < 50f)
            {
                //_vehicleLightSystem.ToggleBrakeLights(true);
                for (int i = 0; i < brakeWheels.Count; i++)
                    brakeWheels[i].brakeTorque = (brakeTorque * 200.0f * brake) * Time.deltaTime;
                _vehicleLightSystem.ToggleBrakeLights(true);
            }
            else if (brake > 0)
            {
                for (int i = 0; i < motorWheels.Count; i++)
                    motorWheels[i].motorTorque = (-reverseTorque * 200.0f * brake) * Time.deltaTime;
            }
            else
            {
                _body.velocity *= Mathf.Exp(-Time.deltaTime);
            }

            if (_isHandBrakeOn)
            {
                for (int i = 0; i < brakeWheels.Count; i++)
                    brakeWheels[i].brakeTorque = brakeTorque;
            }
            
            // Vehicle hand brake
            int handBrake = Convert.ToInt32(_controls.Vehicle.HandBreak.ReadValue<float>());
            
            // Only apply break force if the hand brake action is pressed 
            if (handBrake > 0)
            {  
                _vehicleLightSystem.ToggleBrakeLights(true);
                float handBrakeTorque = (brakeTorque * handBrake) * Time.deltaTime;
                for (int i = 0; i < brakeWheels.Count; i++)
                    brakeWheels[i].brakeTorque = handBrakeTorque;
            }
            else if (handBrake == 0.0f && brake == 0.0f && _vehicleLightSystem.GetBrakeLightsStatus)
            {
                _vehicleLightSystem.ToggleBrakeLights(false);
            }
        }

        private void ApplySteeringAngle(float steeringAngle)
        {
            steeringAngle = steeringAngle * maximumSteerAngle;
            float angle0 = steeringAngle;
            float angle1 = steeringAngle;

            if (Mathf.Abs(steeringAngle) > Mathf.Epsilon)
            {
                float radius = CarBase / Mathf.Tan(steeringAngle * Mathf.Deg2Rad);
                float radius0 = radius - CarWidth * 0.5f;
                float radius1 = radius + CarWidth * 0.5f;

                angle0 = Mathf.Atan(CarBase / radius0) * Mathf.Rad2Deg;
                angle1 = Mathf.Atan(CarBase / radius1) * Mathf.Rad2Deg;
            }

            steeringWheels[0].steerAngle = Mathf.Lerp(steeringWheels[0].steerAngle, angle0, 0.025f);
            steeringWheels[1].steerAngle = Mathf.Lerp(steeringWheels[1].steerAngle, angle1, 0.025f);
        }
        
        private void ApplySteerHelper()
        {
            WheelHit wheelhit;

            // Check if all wheels are actually on the ground and return if not
            foreach (WheelCollider wheel in wheelColliders)
            {
                wheel.GetGroundHit(out wheelhit);
                if (wheelhit.normal == Vector3.zero)
                    return;
            }

            // Avoid gimbal lock problems that will make the car suddenly shift direction
            if (Mathf.Abs(_oldRotation - transform.eulerAngles.y) < 10.0f)
            {
                float turnadjust = (transform.eulerAngles.y - _oldRotation) * steerHelper;
                Quaternion velRotation = Quaternion.AngleAxis(turnadjust, Vector3.up);
                _body.velocity = velRotation * _body.velocity;
            }
            _oldRotation = transform.eulerAngles.y;
        }

        private void ApplyTractionControl()
        {
            WheelHit wheelHit;
            for (int i = 0; i < motorWheels.Count; i++)
            {
                // Maybe this will give an error, if so we change 0 to i
                motorWheels[0].GetGroundHit(out wheelHit);
                if (wheelHit.forwardSlip >= slipLimit && _currentTorque >= 0)
                {
                    _currentTorque -= 10.0f * tractionControl;
                }
                else
                {
                    _currentTorque += 10.0f * tractionControl;
                    if (_currentTorque > fullTorqueOverAllWheels)
                        _currentTorque = fullTorqueOverAllWheels;
                }
            }
        }
        
        private void ApplyDownforce()
        {
            _body.AddForce(-transform.up * downForce * _body.velocity.magnitude);
        }
        
        private void CapSpeed()
        {
            float speed = transform.InverseTransformDirection(_body.velocity).z;
            float pitch;
            
            speed *= 3.6f;

            if (speed >= 0)
                pitch = speed / topSpeed;
            else
                pitch = speed / minimumSpeed;

            _vehicleSound.pitch = _pitchCurve.Evaluate(pitch);
            
            if (speed > topSpeed)
                _body.velocity = (topSpeed / 3.6f) * _body.velocity.normalized;
            else if (speed < minimumSpeed)
                _body.velocity = (minimumSpeed / 3.6f) * -_body.velocity.normalized;
        }

        public void ToggleEngineStatus()
        {
            _isEngineOn = !_isEngineOn;
            if (_isEngineOn)
            {
                _vehicleSound.Play();
            }
            else
            {
                _vehicleSound.Stop();
            }
        }
        
        private void HandBreakOnperformed(InputAction.CallbackContext obj)
        {
            _isHandBrakeOn = !_isHandBrakeOn;
        }

        public float GetCurrentSpeed() => transform.InverseTransformDirection(_body.velocity).z * 3.6f;
    }
}
