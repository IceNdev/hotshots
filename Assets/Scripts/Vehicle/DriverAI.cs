﻿using System;
using UnityEngine;

namespace Vehicle
{
    public class DriverAI : MonoBehaviour
    {
        [SerializeField] private Transform targetPosTranform;
        
        private VehicleController _vehicleController;
        private Vector3 _targetPosition;

        private void Awake()
        {
            _vehicleController = GetComponent<VehicleController>();
            _vehicleController.IsAIControlling = true;
        }

        private void FixedUpdate()
        {
            //SetTargetPosition(targetPosTranform.position);
            if (!_vehicleController.IsEngineOn) 
                return;
            
            float forwardAmount = 1;
            float turnAmount = 1;
            _vehicleController.Move(turnAmount, forwardAmount, 0.0f);
        }

        public void SetTargetPosition(Vector3 targetPosition)
        {
            _targetPosition = targetPosition;
        }
    }
}