﻿using Systems.Quartel;
using Player;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityTemplateProjects;


namespace Vehicle
{
    public class VehicleInteractions : MonoBehaviour
    {
        public Camera playerCamera, vehicleCamera;
        public VehicleCameraManager vehicleCameraManager;
        public GameObject player, vehicle;
        public CharacterController _playerController;
        public GameObject playerUI, vehicleUI;

        private PlayerInActions _controls;
        private bool _isInVehicle;
        private VehicleLightSystem _vehicleLightSystem;
        private VehicleController _vehicleController;
        private VehicleInfo _vehicleInfo;
        private WaterTank _vehicleTank;
        
        private void Awake()
        {
            _controls = PlayerInputs.Instance.PlayerControls;
            
        }
        
        private void StartEngineOnperformed(InputAction.CallbackContext obj)
        {
            _vehicleController.ToggleEngineStatus();
        }
        private void EmergencyLightsOnperformed(InputAction.CallbackContext obj)
        {
            _vehicleLightSystem.ToggleEmergencyLights();
        }
        private void HazardLightsOnperformed(InputAction.CallbackContext obj) =>  _vehicleLightSystem.ToggleHazardLights();

        private void HeadLightsOnperformed(InputAction.CallbackContext obj) => _vehicleLightSystem.ToggleHeadLights();
        
        private void OnEnable() => _controls.Player.Interaction.Enable();
        
        private void OnDisable() => _controls.Player.Interaction.Disable();
                
        // Start is called before the first frame update
        void Start()
        {
            player = gameObject;
            _playerController = player.GetComponent<CharacterController>();
            _controls.Player.Interaction.performed += EnterVehicleOnperformed;

        }

        private bool EnterExitVehicleAction(bool status)
        {
            // Check if the driver seat is occupied
            _vehicleInfo        = vehicle.GetComponent<VehicleInfo>();
            if (status && _vehicleInfo.GetDriverSeatStatus())
            {
                return false;
            }
            
            // Player Stuff
            _isInVehicle = status;
            playerCamera.gameObject.SetActive(!status);
            playerUI.SetActive(!status);
            
            player.GetComponent<PlayerMovementController>().enabled = !status;
            
            // Vehicle Stuff
            vehicleUI.SetActive(status);
            vehicleCameraManager.gameObject.SetActive(status);
            vehicleCameraManager.UpdateChinemachine(vehicle.transform);
            
            vehicle.GetComponent<PlayerInput>().enabled       = status;
            
            _vehicleController  = vehicle.GetComponent<VehicleController>();
            _vehicleLightSystem = vehicle.GetComponent<VehicleLightSystem>();
            _vehicleTank        = vehicle.GetComponent<WaterTank>();
            
            _vehicleInfo.SetGPSStatus(status);
            _vehicleController.enabled = status;
            _vehicleTank.UIStatus = status;
            
            return true;
        }
        
        private void EnterVehicleOnperformed(InputAction.CallbackContext obj)
        {
            Debug.Log($"EnterAction {gameObject.name}");
            if (!_isInVehicle)
            {
                // Bit shift the index of layer (9) to get a bit mask
                int layerMask = 1 << 9;
                Transform cameraTransform = playerCamera.transform;
                RaycastHit hit;
                if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, 3.0f, layerMask))
                {
                    GameObject hitVehicle = hit.collider.gameObject;
                    
                    vehicle = hitVehicle;
                    if (!EnterExitVehicleAction(true))
                    {
                        NotificationManager.Instance.SendNotification("Vehicle", "Vehicle seat is already occupied");
                        return;
                    }
                    vehicle.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                    
                    
                    _vehicleInfo.EnterDriverSeat(true);
                    _controls.Vehicle.HeadLights.performed += HeadLightsOnperformed;
                    _controls.Vehicle.HazardLights.performed += HazardLightsOnperformed;
                    _controls.Vehicle.EmergencyLights.performed += EmergencyLightsOnperformed;
                    _controls.Vehicle.StartEngine.performed += StartEngineOnperformed;
                }
            }
            else
            {
                if (_vehicleController.GetCurrentSpeed() > 10) return;
                
                _controls.Vehicle.HeadLights.performed -= HeadLightsOnperformed;
                _controls.Vehicle.HazardLights.performed -= HazardLightsOnperformed;
                _controls.Vehicle.EmergencyLights.performed -= EmergencyLightsOnperformed;
                _controls.Vehicle.StartEngine.performed -= StartEngineOnperformed;
                
                _vehicleInfo.GetOutDriverSeat(true);
                EnterExitVehicleAction(false);

                _playerController.enabled = false;
                _playerController.transform.position = _vehicleInfo.GetExitPosition();
                _playerController.enabled = true;
                
                vehicle.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                
                _vehicleController = null;
                _vehicleTank = null;
                _vehicleInfo = null;
                _vehicleLightSystem = null;
                vehicle = null;
            }
        }
    }
}
