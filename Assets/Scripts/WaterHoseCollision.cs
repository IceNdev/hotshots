﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterHoseCollision : MonoBehaviour
{
    public ParticleSystem ps;
    public List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();

    void OnParticleCollision(GameObject col)
    {
        int numCollisionEvents = ps.GetCollisionEvents(col, collisionEvents);
        Rigidbody rb = col.GetComponent<Rigidbody>();
        int i = 0;
        Debug.Log(col.tag);

        while (i < numCollisionEvents)
        {
            if (col.tag == "FireMesh")
            {
                Debug.Log("colliding");
                Vector3 pos = collisionEvents[i].intersection;
                Vector3 force = collisionEvents[i].velocity * 10;
                //rb.AddForce(force);
                col.GetComponent<FireCell>().m_HP -= 10;
                col.GetComponent<FireCell>().m_fuel -= 10;
                //col.SetActive(false);
            }
            if (col.tag == "FireNodeHouse")
            {
                Debug.Log("colliding");
                Vector3 pos = collisionEvents[i].intersection;
                Vector3 force = collisionEvents[i].velocity * 10;
                //rb.AddForce(force);
                col.GetComponent<FireNode>().m_HP -= 3;
                col.GetComponent<FireNode>().m_fuel -= 3;
            }
            i++;
        }
        
    }   
}
