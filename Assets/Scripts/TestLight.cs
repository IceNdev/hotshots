﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLight : MonoBehaviour
{
    public Vector2 Tilling;
    private MeshRenderer _renderer;
    private MaterialPropertyBlock _propertyBlock;

    void Start()
    {
        _renderer = GetComponent<MeshRenderer>();
        _propertyBlock = new MaterialPropertyBlock();
    }

    private void OnValidate()
    {
        int propertyID = Shader.PropertyToID("_MainTex_ST");
        
        _renderer = GetComponent<MeshRenderer>();
        _propertyBlock = new MaterialPropertyBlock();
        
        _renderer.GetPropertyBlock(_propertyBlock, 0);
        _propertyBlock.SetVector(propertyID, Tilling);
        //_propertyBlock.SetColor(1, Color.black);
        _renderer.SetPropertyBlock(_propertyBlock, 0);
    }

    void Update()
    {
        _renderer.GetPropertyBlock(_propertyBlock);
        _propertyBlock.SetColor("_Color", Color.Lerp(Color.cyan, Color.green, (Mathf.Sin(Time.time * 2 + 1) + 1) / 2f));
        _renderer.SetPropertyBlock (_propertyBlock);
    }
}
