﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLightController : MonoBehaviour
{
    private Renderer _renderer;
    private bool greenLight = false, yellowLight = false, redLight = false;

    private float timer; // 0-10 green
                         // 10-13 yellow
                         // 13-25 red   

    public bool RedLight => redLight;

    // Start is called before the first frame update
    void Start()
    {
        _renderer = GetComponent<Renderer>();
        _renderer.materials[1].EnableKeyword("_EMISSION");
        greenLight = true;
        yellowLight = false;
        redLight = false;
    }

    void FixedUpdate()
    {
        timer += 1 * Time.deltaTime;

        if (timer <= 2f)
        {
            _renderer.materials[1].EnableKeyword("_EMISSION");
            _renderer.materials[3].DisableKeyword("_EMISSION");
            greenLight = true;
            redLight = false;
        }
        else if (timer <= 5f)
        {
            _renderer.materials[2].EnableKeyword("_EMISSION");
            _renderer.materials[1].DisableKeyword("_EMISSION");
            yellowLight = true;
            greenLight = false;
        }
        else if (timer <= 10)
        {
            _renderer.materials[3].EnableKeyword("_EMISSION");
            _renderer.materials[2].DisableKeyword("_EMISSION");
            redLight = true;
            yellowLight = false;
        }
        else timer = 0f;
    }
}
