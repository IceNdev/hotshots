﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterHoseNPC : MonoBehaviour
{
    public ParticleSystem ps;
    private ParticleSystem.MainModule main;

    [SerializeField] private float particleLifeTime = 10;

    [SerializeField] private float _waterRemoveSpeed = 2.0f;

    private void Start()
    {
        main = ps.main;
    }

    public void Fire(bool status)
    {
        if (status)
        {
            gameObject.SetActive(status);
            //main.startLifetime = particleLifeTime;
        }
        else
        {
            gameObject.SetActive(status);
        }
    }
}
