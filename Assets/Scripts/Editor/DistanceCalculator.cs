﻿using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class DistanceCalculator : EditorWindow
    {
        [MenuItem("Tools/Distance Calculator")]
        public static void Open()
        {
            GetWindow<DistanceCalculator>();
        }
    
        public Transform point1, point2;
        private float _distance;

        private void OnGUI()
        {
            SerializedObject obj = new SerializedObject(this);
        
            EditorGUILayout.PropertyField(obj.FindProperty("point1"));
            EditorGUILayout.PropertyField(obj.FindProperty("point2"));
            EditorGUILayout.LabelField($"Distance: {_distance}");

            if (point1 == null || point2 == null)
            {
                EditorGUILayout.HelpBox("Root transform must be selected. Please assign a root transform.", MessageType.Warning);
            }
            else
            {
                EditorGUILayout.BeginVertical("box");
                DrawButtons();
                EditorGUILayout.EndVertical();
            }

            obj.ApplyModifiedProperties();
        }

        void DrawButtons()
        {
            if (GUILayout.Button("Calculate"))
            {
                Calculate();
            }
        }

        private void Calculate()
        {
            _distance = Vector3.Distance(point1.position, point2.position);
        }
    }
}