﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace NPC
{
    public class RandomTexture : MonoBehaviour
    {
        [SerializeField]
        private Texture[] _textures;

        private static readonly int MainTex = Shader.PropertyToID("_MainTex");

        private void Start()
        {

            gameObject.GetComponent<Renderer>().material.mainTexture = GetRandomTexture();
        }

        private Texture GetRandomTexture()
        {
            int index = Random.Range(0, _textures.Length);
            Debug.Log($"Index: {index}");

            return _textures[index];
        }
    }
}