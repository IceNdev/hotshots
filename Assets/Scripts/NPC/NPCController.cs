﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Systems.Locomotion;
using UnityEngine;
using Collision = Systems.Locomotion.Collision;

public class NPCController : MonoBehaviour
{
    [SerializeField] private Waypoint _waypoint = null;
    private Vector3 destination;
    private Vector3 posToTarget;
    private Vector3 transformPos;
    private Vector3 waypointPos;

    [SerializeField] private float movementSpeed = 1.7f;

    private bool shouldBranch;
    private bool direction; // true -> waypoint.next   |   false -> waypoint.previous
    private bool wait = false;

    [SerializeField] private float offset = .1f;
    [SerializeField] private float _lookAHead = 4.0f;
    [SerializeField] private float _angleRaysWeight = 0.8f;
    [SerializeField] private float _rotationSpeed = 6.0f;
    
    // collision avoidance stuff
    [SerializeField] private List<GameObject> targetList;
    public float threshold = 3, k, maxAccell;
    private Vector3 counterNPCDirection = Vector3.zero;
    private Vector3 avoidObstacle = Vector3.zero;
    [SerializeField] 
    private float sphereRadius = 1.5f;
    [SerializeField]
    private float _avoidDistance = 1f;

    // Animations
    [SerializeField] 
    private Animator _animator;

    void Start()
    {
        //direction = Random.Range(0, 2) >= 1 ? true : false;
        direction = true;
        
        transformPos = new Vector3(transform.position.x, 0, transform.position.z); //transform position with Y=0
        
        if(_waypoint != null)
            SetDestination(_waypoint.GetPosition());
    }
    
    // Update is called once per frame
    void Update()
    {
        if (_waypoint._trafficLightController != null)
            wait = !_waypoint._trafficLightController.RedLight;
        else wait = false;

        if (wait)
        {
            _animator.SetBool("IsWalking", false);
            _animator.SetBool("IsIdle", true);
        }
            

        if (_waypoint.branches != null && _waypoint.branches.Count > 0)
        {
            shouldBranch = Random.Range(0f, 1f) <= _waypoint.branchRatio ? true : false;
        }

        transformPos = new Vector3(transform.position.x, 0, transform.position.z); // Transform position with Y=0
        //waypointPos = new Vector3(destination.x, 0 , destination.z); // waypoint position with Y=0
        posToTarget = waypointPos - transformPos;
        
        // Get the vector that makes NPC get away from others
        foreach (GameObject target in targetList)
        {
            Vector3 direction = transformPos - new Vector3(target.transform.position.x, 0, target.transform.position.z);
            float distanceSqr = direction.sqrMagnitude;
            if (distanceSqr < threshold * threshold)
            {
                float strength = Mathf.Min(maxAccell, k / distanceSqr);
                counterNPCDirection += direction.normalized * strength;
            }
        }
        
        //Collision Detection
        Vector3 safeVector = Vector3.zero;
        Vector3 safePoint = Vector3.zero;
        
        Vector3 ray = transform.forward;
        ray.Normalize();
            
        RaycastHit hit;
        Vector3 transformPosition = transform.position;
        transformPosition.y += 1;

        if (Physics.SphereCast(transform.position, sphereRadius, ray, out hit, _lookAHead))
        {
            Debug.DrawRay(transformPosition, ray*_lookAHead, Color.green);
            safePoint = hit.point + (hit.normal * _avoidDistance);
            safePoint.y = 0;
            safeVector = safePoint - transformPos;
        }

        // Verify if it has reached the destination or is very close
        if (Vector3.Magnitude(posToTarget) <= offset)
        {
            if (direction)
            {
                //transform.forward = _waypoint.transform.forward;
                
                if ((shouldBranch && _waypoint.branches.Count > 0))
                {
                    if (!wait)
                    {
                        _waypoint = _waypoint.branches[0];
                        SetDestination(_waypoint.GetPosition());
                    }
                }
                else
                {
                    if (_waypoint.next != null)
                    {
                        _waypoint = _waypoint.next;
                        SetDestination(_waypoint.GetPosition());
                        
                    }
                    else direction = !direction;
                }
            }
            else
            {
                //transform.forward = -_waypoint.transform.forward;
                
                if (shouldBranch && _waypoint.branches.Count > 0)
                {
                    if (!wait)
                    {
                        _waypoint = _waypoint.branches[1];
                        SetDestination(_waypoint.GetPosition());
                    }
                }
                else
                {
                    if (_waypoint.previous != null)
                    {
                        _waypoint = _waypoint.previous;
                        SetDestination(_waypoint.GetPosition());
                    }
                    else direction = !direction;
                }
            }
        }
        else
        {
            transform.position += (posToTarget.normalized + counterNPCDirection + safeVector) * Time.deltaTime * movementSpeed;
            
            float rotationValue = Mathf.Lerp(Mathf.Atan2(transform.forward.x, transform.forward.z), 
                Mathf.Atan2(posToTarget.x, posToTarget.z), 0.15f);
                    
            transform.rotation = Quaternion.identity;
            transform.Rotate(transform.up, rotationValue * Mathf.Rad2Deg);
            
            _animator.SetBool("IsWalking", true);
            _animator.SetBool("IsIdle", false);
            
            //transform.position += counterNPCDirection * Time.deltaTime * movementSpeed;
        }
        counterNPCDirection = Vector3.zero;
    }

    public void SetDestination(Vector3 destination)
    {
        this.destination = destination;
        waypointPos = new Vector3(this.destination.x, 0, this.destination.z);
        posToTarget = waypointPos - transformPos;
    }
}
