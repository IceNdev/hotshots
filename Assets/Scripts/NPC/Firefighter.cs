﻿using System;
using System.Collections.Generic;
using System.Linq;
using Systems.DecisionTree;
using Systems.Quartel;
using UnityEngine;
using Vehicle;
using Systems.Locomotion;
using Systems.Locomotion.SteeringAlgorithms;
using SaveData;
using UnityEngine.Rendering;
using Utils;
using Collision = Systems.Locomotion.Collision;

namespace NPC
{
    public class Firefighter : MonoBehaviour
    {
        [SerializeField] private float _thresholdVehicle = 3.0f;
        
        [SerializeField]
        private bool _isDriver;
        
        [SerializeField]
        private bool _isInsideCar;

        public bool IsInsideCar => _isInsideCar;

        private List<VehicleInfo> _vehicles;
        private Dictionary<VehicleInfo, float> _vehiclesDistance;
        
        private VehicleInfo _vehicleTarget;
        private int _vehicleIndex;
        private bool _searchOrder;
        private bool _combatFire;

        [SerializeField] 
        private GameObject _bodyModel;
        private CapsuleCollider _collider;

        [SerializeField] 
        private float sphereRadius;
        [SerializeField]
        private float sphereSensorRadius;

        [SerializeField]
        private LayerMask _layerMask;

        [SerializeField]
        private WaterHoseNPC _waterHoseNpc;
        
        private Waypoint _waypoint;
        
        [SerializeField]
        private MovementInfo info;
        [SerializeField]
        private SteeringBehaviour steeringBehaviour;
        [SerializeField,Range(0,1)]
        private float linearDrag = 0.95f, angularDrag = 0.95f;
        [SerializeField,Tooltip("NPC max velocity")]
        private float maxVelocity = 2f;
        
        // Offset of Waypoint
        [SerializeField] 
        private float offset = .1f;

        // Animations
        [SerializeField] 
        private Animator _animator;

        [SerializeField]
        private int _maxFireDistance = 20;

        [SerializeField]
        private int _fireThreshold = 4;

        [SerializeField]
        private float _speed = 6;

        private MissionManager _missionManager;
        
        public Transform groundCheck;
        public float groundDistance = 0.5f;
        private Vector3 _velocity;
        private bool _isGrounded;
        public float gravity = -9.81f;
        public LayerMask groundLayer;

        public MovementInfo Info
        {
            get => info;
            set => info = value;
        }

        // Decision Tree
        private DTAction _searchVehicle, _enterDriverSeat, _enterPassengerSeat, _walkToVehicle, _waypointMove, _putOutFire, _walkToFire, _setToIdle;
        private DTCondition _checkCloseBy, _checkVehicleFull, _checkDriver, _checkDriverSeat, _canWalkToCar, _checkFireDistance, _checkFireCloseBy;

        private void Start()
        {
            _missionManager = GameObject.Find("MissionManager").GetComponent<MissionManager>();
            _collider = GetComponent<CapsuleCollider>();
            
            //Start of animation process
            _animator.SetBool("IsWalking", false);
            _animator.SetBool("IsIdle", true);

            info.position = transform.position;
            Vector3 forward = transform.forward;
            info.orientation = Mathf.Atan2(forward.x, forward.z);

            steeringBehaviour = Instantiate(steeringBehaviour);
            steeringBehaviour.Init();

            _vehiclesDistance = new Dictionary<VehicleInfo, float>();
            _searchVehicle = new DTAction(() =>
            {
                _vehicleIndex++;
                SearchForVehicle();
            });
            _enterDriverSeat = new DTAction(() =>
            {
                _searchOrder = false;
                EnterVehicle(true);
                _vehicleTarget.EnterDriverSeat(false);
            });
            _enterPassengerSeat = new DTAction(() =>
            {
                _searchOrder = false;
                EnterVehicle(true);
                _vehicleTarget.EnterPassengerSeat();
            });
            _walkToVehicle = new DTAction(() =>
            {
                Vector3 target = _vehicleTarget.transform.position;
                Move(target);
            });
            _waypointMove = new DTAction(() =>
            {
                WaypointMove();
            });
            _putOutFire = new DTAction(() =>
            {
                PutOutFire();
            });
            _walkToFire = new DTAction(() =>
            {
                Vector3 target = _missionManager.FirePos;
                Move(target);
            });
            _setToIdle = new DTAction(() =>
            {
                SetIdleAnimation();
            });

            _checkDriverSeat    = new DTCondition(CheckDriverSeat, _searchVehicle, _enterDriverSeat);
            _checkVehicleFull   = new DTCondition(CheckVehicleFull, _searchVehicle, _enterPassengerSeat);
            _checkDriver        = new DTCondition(CheckDriver, _checkDriverSeat, _checkVehicleFull);
            _checkCloseBy       = new DTCondition(CheckCloseBy, _checkDriver, _walkToVehicle);
            _canWalkToCar       = new DTCondition(CanWalkToCar, _checkCloseBy, _waypointMove);
            _checkFireDistance  = new DTCondition(CheckFireDistance, _putOutFire, _walkToFire);
            _checkFireCloseBy   = new DTCondition(CheckFireCloseBy, _checkFireDistance, _setToIdle);
        }

        private bool CheckDriverSeat() => _vehicleTarget.GetDriverSeatStatus();

        public bool CanWalkToCar()
        {
            return _waypoint == null;
        }

        private bool CheckCloseBy()
        {
            float distance = Vector3.Distance(_vehicleTarget.transform.position, transform.position);
            return distance <= _thresholdVehicle;
        }

        private bool CheckVehicleFull() => _vehicleTarget.IsFull();
        private bool CheckDriver() => _isDriver;

        private bool CheckFireDistance()
        {
            Vector3 missionPos = _missionManager.FirePos;
            //missionPos.y = transform.position.y;
            
            return Vector3.Distance(missionPos, transform.position) <= _fireThreshold;
        }

        private bool CheckFireCloseBy()
        {
            Vector3 missionPos = _missionManager.FirePos;
            //missionPos.y = transform.position.y;
            
           return  Vector3.Distance(missionPos, transform.position) <= _maxFireDistance;
        }

        public bool IsDriver => _isDriver;

        private void Update()
        {
            _velocity.y += gravity * Time.deltaTime;
            
            _isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundLayer);
            if (_isGrounded && _velocity.y < 0.0f)
                _velocity.y = 0f;

            transform.position += _velocity;
            
            if (_searchOrder == true)
            {
                //_waypoint = GetClosestWaypoint();
                _canWalkToCar.Run();
            }

            if (_combatFire == true && _missionManager.mission_active)
            {
                _checkFireCloseBy.Run();
            }
            else
            {
                _waterHoseNpc.Fire(false);
            }
        }

        public void SearchForVehicle()
        {
            _vehicles = QuartelManager.Instance.GetVehicles();
            
            _searchOrder = true;
            foreach (VehicleInfo vehicle in _vehicles)
            {
                if (!_vehiclesDistance.ContainsKey(vehicle))
                {
                    float distance = Vector3.Distance(vehicle.transform.position,transform.position);
                    _vehiclesDistance.Add(vehicle, distance);
                }
                else
                {
                    float distance = Vector3.Distance(vehicle.transform.position, transform.position);
                    _vehiclesDistance[vehicle] = distance;
                }
            }

            if (_vehiclesDistance.Count > 0)
                _vehiclesDistance.OrderBy(x => x.Value);
            
            _vehicleTarget = _vehiclesDistance.ElementAt(_vehicleIndex).Key;
        }

        public void SetWaypoint()
        {
            _waypoint = GetClosestWaypoint();
        }

        private void Move(Vector3 position)
        {
            MovementInfo target = new MovementInfo { position = position };

            // Update our position and our orientation 
            info.position    += info.velocity * Time.deltaTime;
            info.orientation += info.rotation * Time.deltaTime;

            // Get the movement steering data, and update our velocities
            Steering steering = steeringBehaviour.GetSteering(info, target);
            info.velocity += steering.linear;
            info.rotation += steering.angular;

            // Apply drag
            info.velocity *= linearDrag;
            info.rotation *= angularDrag;

            // Do not exceed our max velocity
            info.velocity = Vector3.ClampMagnitude(info.velocity, maxVelocity);

            // Do not forget to transform radians into degrees
            //info.orientation = MathHelpers.NormAngle(info.orientation);
            
            
            info.orientation = Mathf.Lerp(info.orientation, Mathf.Atan2(info.velocity.x, info.velocity.z), 0.45f);
            //info.orientation = Mathf.Atan2(info.velocity.x, info.velocity.z);
            
            transform.rotation = Quaternion.identity;
            
            transform.Rotate(transform.up, info.orientation * Mathf.Rad2Deg);
            
            transform.position = info.position;

            _animator.SetBool("IsWalking", true);
            _animator.SetBool("IsIdle", false);
            //transform.forward = info.velocity;
        }

        private void EnterVehicle(bool status)
        {
            _bodyModel.SetActive(!status);
            _collider.enabled = !status;
            _isInsideCar = status;
            
            if (status)
            {
                gameObject.transform.SetParent(_vehicleTarget.transform);
            }
            else
            {
                gameObject.transform.parent = null;
            }
            
        }
        
        private Waypoint GetClosestWaypoint()
        {
            Waypoint bestWaypoint = null;
            float closestDistanceSqr = Mathf.Infinity;
            Vector3 currentPosition = transform.position;
            RaycastHit[] objectsResult = Physics.SphereCastAll(currentPosition, sphereRadius, Vector3.forward,
                sphereSensorRadius, _layerMask);

            for (int i = 0; i < objectsResult.Length; i++)
            {
                RaycastHit potentialTarget = objectsResult[i];
                Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    bestWaypoint = potentialTarget.collider.gameObject.GetComponent<Waypoint>();
                }
            }

            return bestWaypoint;
        }

        private void WaypointMove()
        {
            Vector3 moveVector = Vector3.zero;
            Vector3 waypointPos = new Vector3(_waypoint.transform.position.x, 0, _waypoint.transform.position.z);
            Vector3 transformPos = new Vector3(transform.position.x, 0, transform.position.z);

            if (_waypoint != null)
            {
                if (_waypoint.previous == null)
                    moveVector = waypointPos - transformPos;
                else if (_waypoint.previous != null &&
                         _waypoint.previous.transform.position.y == _waypoint.transform.position.y)
                    moveVector = waypointPos - transformPos;
                else if (_waypoint.previous.transform.position.y != _waypoint.transform.position.y)
                    moveVector = _waypoint.GetPosition() - transform.position;

                if (Vector3.Magnitude(moveVector) <= offset)
                    _waypoint = _waypoint.next;
                else
                {
                    transform.position += moveVector.normalized * Time.deltaTime * _speed;
                    _animator.SetBool("IsWalking", true);
                    _animator.SetBool("IsIdle", false);
                    
                    float rotationValue = Mathf.Lerp(Mathf.Atan2(transform.forward.x, transform.forward.z), 
                        Mathf.Atan2(_waypoint.transform.forward.x, _waypoint.transform.forward.z), 0.15f);
                    
                    transform.rotation = Quaternion.identity;
                    transform.Rotate(transform.up, rotationValue * Mathf.Rad2Deg);
                    //transform.forward = _waypoint.transform.forward;
                    info.position = transform.position;
                }
            }
        }

        private void SetIdleAnimation()
        {
            _animator.SetBool("IsWalking", false);
            _animator.SetBool("IsIdle", true);
        }

        private void PutOutFire()
        {
            SetIdleAnimation();
            _waterHoseNpc.Fire(true);
        }

        public void SetCombatFire(bool value)
        {
            _combatFire = value;
        }

        public void ExitVehicle()
        {
            transform.position = _vehicleTarget.GetExitPosition();
            info.position = transform.position;

            if(_isDriver)
                _vehicleTarget.GetOutDriverSeat(false);
            else
                _vehicleTarget.GetOutPassengerSeat();
            
            EnterVehicle(false);
        }
    }
}