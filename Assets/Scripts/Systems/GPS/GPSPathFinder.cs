﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityTemplateProjects;

namespace Systems.GPS
{
    public class PathfindingList
    {
        [SerializeField] private List<GPSNode> _gpsNodes;

        public PathfindingList()
        {
            _gpsNodes = new List<GPSNode>();
        }
        public GPSNode Cheapest() => _gpsNodes.OrderBy(x => x.FCost).First();
        public GPSNode Find(Vector2 position)
        {
            return _gpsNodes.Find(x => x.Position == position);
        }
        public void Add(GPSNode node) => _gpsNodes.Add(node);
        public void Remove(GPSNode node) => _gpsNodes.Remove(node);
        public bool Contains(GPSNode node) => _gpsNodes.Contains(node);
        public GPSNode ElementAt(int index) => _gpsNodes.ElementAt(index);
        public bool IsLast(GPSNode node) => _gpsNodes.Last() == node;
        public void RemoveBefore(GPSNode node)
        {
            int index = _gpsNodes.FindIndex(x => x == node);
            _gpsNodes.RemoveRange(0, index);
        }
        public int Count => _gpsNodes.Count;

        public void Reverse() => _gpsNodes.Reverse();
    }
    
    [RequireComponent(typeof(LineRenderer))]
    public class GPSPathFinder : MonoBehaviour
    {
        [SerializeField] private GPSNode _start;
        [SerializeField] private Transform _end;

        [SerializeField] private float _gpsPointHeight = 0.4f;
        [SerializeField] private float _gpsSensorRadius = 80.0f;
        [SerializeField] private LayerMask _gpsSensorLayer;
        [SerializeField] private LayerMask _gpsHeightLayer;
        [SerializeField] private float _gpsTimer = 1.5f;
        [SerializeField] private UnityEvent _newMissionEvent;
        
        private LineRenderer _lineRenderer;
        private MissionManager _missionManager;
        private bool _functionExecuted = false;
        
        public void SetDestination(Vector2 position)
        {
            _end.position = position;
        }

        public List<Vector2> PathFindAStar(GPSNode start, Vector2 end)
        {
            GPSNode current = null;
            PathfindingList closed = new PathfindingList();
            PathfindingList open = new PathfindingList();

            open.Add(start);

            while (open.Count > 0)
            {
                current = open.Cheapest();
                if (current.HCost == 0)
                    current.CalculateHCost(end);
                if (current.Position == end)
                {
                    open.Remove(current);
                    closed.Add(current);
                    break;
                }

                GPSConnection[] connections = current.GetConnections();

                foreach (GPSConnection connection in connections)
                {
                    Vector2 connectionPosition = connection.Node.Position;

                    int connectionNodeCost = current.GCost + connection.GetGost();
                    float connectionNodeH = 0.0f;

                    GPSNode connectionNode = closed.Find(connectionPosition);
                    if (connectionNode != null) continue;

                    connectionNode = open.Find(connectionPosition);
                    if (connectionNode != null) continue;

                    connectionNode = connection.Node;

                    connectionNode.GCost = connectionNodeCost;
                    connectionNode.CalculateHCost(end);
                    connectionNode.FromNode = current;

                    open.Add(connectionNode);
                }

                open.Remove(current);
                closed.Add(current);
            }

            if (current.Position != end) return null;

            List<Vector2> path = new List<Vector2>();

            while (current != start)
            {
                path.Add(current.Position);
                current = closed.Find(current.FromNode.Position);
            }
            path.Add(start.Position);
            path.Reverse();
            return path;
        }

        private void Start()
        {
            _lineRenderer = GetComponent<LineRenderer>();
            _missionManager = GameObject.Find("MissionManager").GetComponent<MissionManager>();
        }

        private void Update()
        {
            if (_missionManager.mission_active && !_functionExecuted)
            {
                _newMissionEvent.Invoke();
                _functionExecuted = true;
            }
        }

        public void ShowGPSPath()
        {
            if (_missionManager == null)
            {
                Debug.LogError("Can't find mission manager");
                return;
            }
            StopCoroutine(nameof(FindPathCoroutine));
            StartCoroutine(FindPathCoroutine());
        }

        private IEnumerator FindPathCoroutine()
        {
            GPSNode start = null;
            do
            {
                start = GetClosestNode();
                _end = _missionManager.GetMissionTransform();
                if (start != null && _end != null)
                {
                    Vector2 end = new Vector2(_end.position.x, _end.position.z);
                    List<Vector2> nodes = PathFindAStar(start, end);
                    _lineRenderer.positionCount = nodes.Count;

                    for (int i = 0; i < nodes.Count; i++)
                    {
                        Vector3 nodePosition = new Vector3(nodes[i].x, 10.0f, nodes[i].y);
                        float y = 0.0f;

                        if (Physics.Raycast(nodePosition, Vector3.down, out RaycastHit hit, 90.0f, _gpsHeightLayer))
                        {
                            y = hit.point.y;
                        }

                        Vector3 position = new Vector3(nodes[i].x, y + _gpsPointHeight, nodes[i].y);
                        _lineRenderer.SetPosition(i, position);
                    }
                }
                else
                {
                    _lineRenderer.positionCount = 0;
                }
                yield return new WaitForSeconds(_gpsTimer);
            } while (_lineRenderer.positionCount > 1 || _missionManager.mission_active);
            _functionExecuted = false;
            _lineRenderer.positionCount = 0;
            yield return null;
        }

        private GPSNode GetClosestNode()
        {
            GPSNode bestNode = null;
            float closestDistanceSqr = Mathf.Infinity;
            Vector3 currentPosition = transform.position;
            RaycastHit[] objectsResult = Physics.SphereCastAll(currentPosition, _gpsSensorRadius, Vector3.forward,
                _gpsSensorRadius, _gpsSensorLayer, QueryTriggerInteraction.Collide);

            if (objectsResult == null)
                return null;
            
            for (int i = 0; i < objectsResult.Length; i++)
            {
                RaycastHit potentialTarget = objectsResult[i];
                Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    bestNode = potentialTarget.collider.gameObject.GetComponent<GPSNode>();
                }
            }

            return bestNode;
        }

        public void SetActive(bool status)
        {
            _functionExecuted = false;
            gameObject.SetActive(status);
        }
    }
}
