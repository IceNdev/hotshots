﻿using System;
using System.Linq;
using UnityEngine;

namespace Systems.GPS
{
    [Serializable]
    public class GPSConnection
    {
        public GPSNode Node;
        public Vector2 FromPosition;

        /*
        public GPSConnection(Vector2 position)
        {
            FromPosition = position;
        }*/
        
        public int GetGost() => (int) (FromPosition - Node.Position).sqrMagnitude;
        
    }
    
    public class GPSNode : MonoBehaviour
    {
        [SerializeField] 
        private GPSConnection[] _connections;

        public GPSNode Cheapest() => _connections.ToList().OrderBy(x => x.Node.FCost).First().Node;
        public GPSNode Cheapest(GPSNode node) => _connections.ToList().OrderBy(x => x.Node.FCost).First(x => x.Node != node).Node;
        public GPSNode Cheapest(GPSNode node, GPSNode node2) => _connections.ToList().OrderBy(x => x.Node.FCost).First(x => x.Node != node && x.Node != node2).Node;

        public bool IsOneWay() => _connections.Length < 2;
        
        public Vector2 Position => new Vector2(transform.position.x, transform.position.z);

        public int GCost;
        public int HCost;
        public int FCost => GCost + HCost;

        public GPSNode FromNode;
        public GPSConnection[] GetConnections() => _connections;

        public void CalculateHCost(Vector2 target)
        {
            HCost = (int) (Position - target).sqrMagnitude;
        }
 
        private void Awake()
        {
            for (int i = 0; i < _connections.Length; i++)
            {
                Vector2 position = new Vector2(transform.position.x, transform.position.z);
                //_connections[i] = new GPSConnection(position);
                _connections[i].FromPosition = position;

            }
        }
    }
}
