﻿using UnityEngine;

namespace Systems.Locomotion
{
    public class Collision
    {
        private Vector3 _point, _normal;
        public Collision(Vector3 point, Vector3 normal)
        {
            _point  = point;
            _normal = normal;
        }

        public Vector3 Point => _point;
        public Vector3 Normal => _normal;
    }
    public interface ICollisionDetector
    {
        Collision GetCollision(Vector3 origin, Vector3 ray);
    }

    public static class CollDetector
    {
        public static Collision GetCollision(Vector3 origin, Vector3 ray)
        {
            RaycastHit hit;
            if (Physics.Raycast(origin, ray, out hit, ray.magnitude))
            {
                if (!hit.collider.CompareTag("FirefighterNPC"))
                {
                    Debug.DrawRay(origin, ray);
                    return new Collision(hit.point, hit.normal);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
            
        }
    }
}