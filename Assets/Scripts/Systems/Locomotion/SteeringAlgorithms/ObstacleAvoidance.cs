﻿using UnityEngine;

namespace Systems.Locomotion.SteeringAlgorithms
{
    [CreateAssetMenu(menuName="Behaviour/ObstacleAvoidance")]
    public class ObstacleAvoidance : Seek
    {
        [SerializeField] 
        private float _avoidDistance = 1.5f;
        [SerializeField]
        private float _lookAHead = 4.0f;
        [SerializeField] 
        private float sphereRadius = 1.5f;
        [SerializeField]
        private LayerMask _layerMask;
        //[SerializeField] private LayerMask _layerMask;

        private MovementInfo npcTest;
        
        public override Steering GetSteering(MovementInfo npc, MovementInfo target)
        {
            //npcTest = npc;
            Vector3 safeVector = Vector3.zero;
            Vector3 safePoint = Vector3.zero;
            Vector3 ray = npc.velocity;
            ray.Normalize();

            //Vector3 sphereCenter = npc.position + new Vector3(0f, 0.4f, 0f);

            //MovementInfo newTarget = new MovementInfo();
            
            RaycastHit hit;

            if (Physics.SphereCast(npc.position, sphereRadius, ray, out hit, _lookAHead, _layerMask))
            {
                Debug.DrawRay(npc.position, ray*_lookAHead, Color.green);
                //Debug.Log($"hit name: {hit.transform.name}, AD: {_avoidDistance}");
                safePoint = hit.point + (hit.normal * _avoidDistance);
                target.position = safePoint;
                //Vector3 newTargetPos = new Vector3(target.position.x, 0, target.position.z);
                //Vector3 newNPCpos = new Vector3(npc.position.x, 0, npc.position.z);
                //npc.velocity = newTargetPos - newNPCpos;
                return base.GetSteering(npc, target);
            }
            //else return new Steering();

            //target.position = safePoint;
            //.position += safeVector * _avoidDistance;

            return base.GetSteering(npc, target);
        }
    }
}