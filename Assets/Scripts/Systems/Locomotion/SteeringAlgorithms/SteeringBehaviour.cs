﻿using UnityEngine;

namespace Systems.Locomotion.SteeringAlgorithms
{
    public abstract class SteeringBehaviour : ScriptableObject
    {
        public virtual SteeringBehaviour Init()
        {
            return this;
        }

        public abstract Steering GetSteering(MovementInfo npc, MovementInfo target);
    }
}