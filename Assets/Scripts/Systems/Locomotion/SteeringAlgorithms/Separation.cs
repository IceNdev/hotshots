﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Systems.Quartel;
using NPC;
using UnityEngine;

namespace Systems.Locomotion.SteeringAlgorithms
{
    [CreateAssetMenu(menuName="Behaviour/Separation")]
    public class Separation : SteeringBehaviour
    {
        private List<Firefighter> targetList;
   
        public float threshold = 3, k, maxAccell;
    
        public override SteeringBehaviour Init()
        {
            targetList = QuartelManager.Instance.GetFirefighters();
            return this;
        }

        public override Steering GetSteering(MovementInfo npc, MovementInfo ignoredTarget)
        {
            Steering steering = new Steering();
            foreach (Firefighter target in targetList)
            {
                Vector3 direction = npc.position - target.Info.position;
                float distanceSqr = direction.sqrMagnitude;
                if (distanceSqr < threshold * threshold)
                {
                    float strength = Mathf.Min(maxAccell, k / distanceSqr);
                    steering.linear += direction.normalized * strength;
                }
            }
            steering.linear.y = 0;
            
            return steering;
        }
    }

}