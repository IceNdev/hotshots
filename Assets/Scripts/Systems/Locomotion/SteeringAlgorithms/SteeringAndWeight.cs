﻿using System;

namespace Systems.Locomotion.SteeringAlgorithms
{
    [Serializable]
    public class SteeringAndWeight
    {
        public float weight;
        public SteeringBehaviour behaviour;
    }
}