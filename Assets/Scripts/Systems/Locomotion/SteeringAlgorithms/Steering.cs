﻿using UnityEngine;

namespace Systems.Locomotion.SteeringAlgorithms
{
    public class Steering 
    {
        // linear force
        public Vector3 linear;
        // angular force
        public float angular;
    }
}