﻿using UnityEngine;

namespace Systems.Locomotion.SteeringAlgorithms
{
    [CreateAssetMenu(menuName="Behaviour/Seek")]
    public class Seek : SteeringBehaviour
    {
        [SerializeField]
        float maxAccel = 3f;

        public override Steering GetSteering(MovementInfo npc, MovementInfo target) {
            // Direction vector, from npc to target
            Vector3 direction = target.position - npc.position;
        
            Steering steering = new Steering();
            steering.linear = direction.normalized * maxAccel;
            steering.linear.y = 0;

            return steering;
        }
        
        public Steering GetSteering(MovementInfo npc) {
            // Direction vector, from npc to target
            Vector3 direction = npc.velocity;
        
            Steering steering = new Steering();
            steering.linear = direction.normalized * maxAccel;
            steering.linear.y = 0;
            
            return steering;
        }
    }
}