﻿using System;
using System.Collections.Generic;
using Systems.Quartel;
using NPC;
using UnityEngine;
using Utils;
using Vehicle;

namespace Systems
{
    public class AIManager : Singleton<AIManager>
    {
        private List<Firefighter> _fireFighters;
        

        private void Start()
        {
            _fireFighters = QuartelManager.Instance.GetFirefighters();
        }
        
        public void EnterVehicle()
        {
            foreach (Firefighter f in _fireFighters.FindAll(x => !x.IsInsideCar))
            {
                f.SearchForVehicle();
                f.SetWaypoint();
            }
        }

        public void ExitVehicle()
        {
            foreach (Firefighter f in _fireFighters.FindAll(x => x.IsInsideCar))
            {
                f.ExitVehicle();
            }
        }

        public void CombatFire()
        {
            foreach (Firefighter f in _fireFighters)
            {
                f.SetCombatFire(true);
            }
        }
    }
}