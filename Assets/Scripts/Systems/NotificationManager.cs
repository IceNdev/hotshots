﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UnityTemplateProjects
{
    public class NotificationManager : MonoBehaviour
    {
        public static NotificationManager Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                _instance = FindObjectOfType<NotificationManager>();

                if (_instance != null)
                    return _instance;

                CreateNewInstance();
                return _instance;
            }
        }

        public static NotificationManager CreateNewInstance()
        {
            NotificationManager notificationManagerPrefab = Resources.Load<NotificationManager>("NotificationManager");
            _instance = Instantiate(notificationManagerPrefab);

            return _instance;
        }

        [SerializeField] 
        public TextMeshProUGUI titleText = null;
        [SerializeField] 
        public TextMeshProUGUI descriptionText = null;

        [SerializeField] 
        private GameObject backgroundTitle = null;
        [SerializeField]
        private GameObject backgroundDescription = null;

       private Animation animation;

        [SerializeField] 
        private float fadeTime = 3f;

        private IEnumerator _notificationCoroutine = null;
        
        private static NotificationManager _instance = null;

        private void Start()
        {
            //animation = GetComponent<Animation>();
        }
        private void Awake()
        {
            if(Instance != this)
                Destroy(gameObject);
        }

        public void SendNotification(string title, string description, float time = 3.0f)
        {
            if(Math.Abs(fadeTime - 3.0f) > 0.001f)
                fadeTime = time;
            if(_notificationCoroutine != null)
                StopCoroutine(_notificationCoroutine);
            
            backgroundTitle.SetActive(true);
            
            _notificationCoroutine = FadeOutNotification(title, description);
            StartCoroutine(_notificationCoroutine);
            
            
            //animation.Play();
        }

        private IEnumerator FadeOutNotification(string title, string description)
        {
            Image titleImage = backgroundTitle.GetComponent<Image>();
            Image descriptionImage = backgroundDescription.GetComponent<Image>();
            titleText.SetText(title);
            descriptionText.SetText(description);
            float t = 0f;
            while (t < fadeTime)
            {
                t += Time.unscaledDeltaTime;
                float alphaText = Mathf.Lerp(1f, 0f, t / fadeTime);
                float alphaImage = Mathf.Lerp(0.9f, 0f, t / fadeTime);
                
                FadeOutObject(ref titleText, alphaText);
                FadeOutObject(ref descriptionText, alphaText);
                
                FadeOutObject(ref titleImage, alphaImage);
                FadeOutObject(ref descriptionImage, alphaImage);
                yield return null;
                
            }
            backgroundTitle.SetActive(false);
        }

        private void FadeOutObject(ref TextMeshProUGUI text, float alpha)
        {
            text.color = new Color(
                text.color.r,
                text.color.g,
                text.color.b,
                alpha);
        }
        
        private void FadeOutObject(ref Image image, float alpha)
        {
            image.color = new Color(
                image.color.r,
                image.color.g,
                image.color.b,
                alpha);
        }
    }
}