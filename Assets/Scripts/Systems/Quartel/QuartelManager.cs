﻿using System;
using System.Collections;
using System.Collections.Generic;
using NPC;
using SaveData;
using UnityEngine;
using Utils;
using Vehicle;

namespace Systems.Quartel
{
    public enum EBuyFireResult
    {
        Vehicles,
        Beds,
        Success
    }
    public class QuartelManager : Singleton<QuartelManager>
    {
        [SerializeField]
        private List<Garage> _garages = new List<Garage>();
        
        [SerializeField]
        private List<SpawnBed> _spawnBeds = new List<SpawnBed>();
        
        private List<Firefighter> _firefighters = new List<Firefighter>();

        [SerializeField] 
        private GameObject _quartelUI;

        public bool IsUIActive() => _quartelUI.activeSelf;

        private static int VehicleSeats;
        private static int VehicleDriverSeats = 0;
        
        public void AddGarage(Garage garage) => _garages.Add(garage);

        public bool AddCarToGarage(GameObject vehicleObject)
        {
            VehicleInfo vehicle = vehicleObject.GetComponent<VehicleInfo>();
            // Get a garage
            Garage garage = _garages.Find(x => !x.IsOccupied && x.VehicleSize == vehicle.SizeType);

            if (garage != null)
            {
                GameObject newObject = Instantiate(vehicleObject, garage.GetPosition(), garage.GetRotation());
                garage.AddVehicle(newObject.GetComponent<VehicleInfo>());
                VehicleDriverSeats += 1;
                VehicleSeats += vehicle.Slots; 
                return true;
            }
            return false;
        }
        
        public EBuyFireResult AddFirefighter(GameObject firefighter)
        {
            SpawnBed spawnBed = _spawnBeds.Find(x => !x.IsOccupied);

            if (spawnBed != null)
            {
                bool isDriver = firefighter.GetComponent<Firefighter>().IsDriver;
                // Firefighter Driver
                if (isDriver && VehicleDriverSeats > 1)
                {
                    VehicleDriverSeats--;
                }
                else if (isDriver && VehicleDriverSeats <= 1)
                {
                    return EBuyFireResult.Vehicles;
                }
                // Firefighter normal
                if (!isDriver && VehicleSeats > 0)
                {
                    VehicleSeats--;
                }
                else if(!isDriver && VehicleSeats == 0)
                {
                    return EBuyFireResult.Vehicles;
                }
                
                GameObject newObject = Instantiate(firefighter, spawnBed.GetPosition(), spawnBed.GetRotation());
                spawnBed.AddFirefighter(newObject.GetComponent<Firefighter>());
                return EBuyFireResult.Success;
            }

            return EBuyFireResult.Beds;
        }
        
        public List<Firefighter> GetFirefighters()
        {
            foreach (SpawnBed bed in _spawnBeds)
            {
                if (bed.Firefighter == null)
                    continue;
                if(bed.IsOccupied)
                    _firefighters.Add(bed.Firefighter);
            }
            return _firefighters;
        }
        public int GetVehiclesCapacityFree()
        {
            int capacityFree = _garages.FindAll(garage => garage.GetVehicle() == null).Count;
            return capacityFree;
        }

        public List<Garage> GetGarages()
        {
            List<Garage> garages = new List<Garage>();
            foreach (Garage garage in _garages)
            {
                if(garage.IsOccupied)
                    garages.Add(garage);
            }

            return garages;
        }

        public List<VehicleInfo> GetVehicles()
        {
            List<VehicleInfo> vehicles = new List<VehicleInfo>();
            foreach (Garage garage in _garages)
            {
                if(garage.IsOccupied)
                    vehicles.Add(garage.GetVehicle());
            }
            return vehicles;
        }

        public List<SpawnBed> GetSpawnBeds() => _spawnBeds;

        public void RefillTank(WaterTank waterTank, int value) => waterTank.AddWater(value);
        public void EmptyTank(WaterTank waterTank, int value) => waterTank.RemoveWater(value);

        public void SetUIStatus(bool status)
        {
            if (status)
            {
                _quartelUI.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                _quartelUI.SetActive(false);
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
        
        public void UpdateGarage(VehicleInfo vehicleInfo, Vector3 position)
        {
            Garage garage = _garages.Find(x => x.GetPosition() == position);
            if (garage != null)
            {
                garage.AddVehicle(vehicleInfo);
                VehicleDriverSeats += 1;
                VehicleSeats += vehicleInfo.Slots;
            }
        }
        
        public void UpdateSpawnBed(Firefighter firefighter, Vector3 position)
        {
            SpawnBed spawnBed = _spawnBeds.Find(x => x.GetPosition() == position);
            if (spawnBed != null)
            {
                spawnBed.AddFirefighter(firefighter);
            }
        }

        private void OnDestroy()
        {
            VehicleSeats       = 0;
            VehicleDriverSeats = 0;
        }
    }
}

