﻿using UnityEngine;

namespace Systems.Quartel
{
    public class WaterTankSlot
    {
        [SerializeField]
        private WaterTank _waterTank;

        public bool IsOccupied => _waterTank != null;
    }
}