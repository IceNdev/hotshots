﻿using System;
using Player;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;

namespace Systems.Quartel
{
    public class WaterTank : MonoBehaviour
    {
        // If is fixed, doesn't need to have actions
        [SerializeField]
        private bool _fixed;

        private bool _uiStatus;
        private bool _workStatus = false;
        
        // Tank Stats
        [SerializeField]
        private float _capacityMax = 5000;
        
        [SerializeField]
        private float _waterLoaded = 100;

        private WaterPump _waterPump;

        public Hose Hose { get; set; }
        
        // UI
        private ProgressBar _waterProgressBar;
        private TextMeshProUGUI _waterPumpText;
        
        // Input
        private PlayerInActions _controls;

        public void AddWater(float value) => _waterLoaded += value * Time.deltaTime;
        public void RemoveWater(float value) => _waterLoaded -= value * Time.deltaTime;
        public float FreeCapacity() => _capacityMax - _waterLoaded;
        public float CapacityMax => _capacityMax;

        public bool UIStatus
        {
            get => _uiStatus;
            set => _uiStatus = value;
        }

        public float WaterLoaded => _waterLoaded;

        private void Awake()
        {
            if (_fixed) return;
            _controls = PlayerInputs.Instance.PlayerControls;
        }

        private void Start()
        {
            _waterPump = new WaterPump();
            if (_fixed) return;
            _controls.Vehicle.RefillTank.performed += RefillTankOnperformed;
            _controls.Vehicle.PumpWorkMode.performed += PumpWorkModeOnperformed;
        }

        private void Update()
        {
            float waterPercent = (float)_waterLoaded / _capacityMax;

            if (!_fixed && _uiStatus)
            {
                if (_waterProgressBar == null) return;
                _waterProgressBar.SetProgressBar(waterPercent);
            }
                
            
            if (Hose == null) return;
            if (_workStatus)
            {
                _waterPump.tankInput = Hose.WaterTank1;
                _waterPump.tankOutput = Hose.WaterTank2;

                // TODO: FIX THE REFILL FOR FIXED OBJECTS
                if (_waterPump.Work() && _fixed)
                {
                    _waterLoaded = _capacityMax;
                }
            }
        }

        public void UpdateUI(ProgressBar progressBar, TextMeshProUGUI pumpMode)
        {
            _waterProgressBar = progressBar;
            _waterPumpText = pumpMode;
            _waterPumpText?.SetText(_waterPump.Mode.ToString());
        }
        
        private void RefillTankOnperformed(InputAction.CallbackContext obj)
        {
            _workStatus = !_workStatus;
        }
        
        private void PumpWorkModeOnperformed(InputAction.CallbackContext obj)
        {
            _waterPump.ToggleWorkMode();
            
            _waterPumpText?.SetText(_waterPump.Mode.ToString());
        }

        private WaterTank GetClosestTank()
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, 10f);

            foreach (Collider collider in colliders)
            {
                if (collider.CompareTag("WaterTank") || collider.CompareTag("Truck"))
                {
                    WaterTank waterTank = collider.gameObject.GetComponent<WaterTank>();
                    if (waterTank != null)
                    {
                        return waterTank;
                    }
                }
            }
            return null;
        }

        public void SetWater(float value) => _waterLoaded = value;
    }
}