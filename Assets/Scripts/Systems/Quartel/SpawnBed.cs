﻿using NPC;
using UnityEngine;

namespace Systems.Quartel
{
    public class SpawnBed : MonoBehaviour
    {
        public Vector3 GetPosition() => transform.position;
        public Quaternion GetRotation() => transform.rotation;
        public Firefighter Firefighter;
        
        public bool IsOccupied => Firefighter != null;

        public void AddFirefighter(Firefighter firefighter)
        {
            Firefighter = firefighter;
        }
    }
}