﻿using System;
using UnityEngine;
using Vehicle;

namespace Systems.Quartel
{
    [Serializable]
    public enum EVehicleSize
    {
        Small,
        Medium,
        Big
    }
    [Serializable]
    public class Garage : MonoBehaviour
    {
        [SerializeField]
        private EVehicleSize _sizeType;
        public Vector3 GetPosition() => transform.position;
        public Quaternion GetRotation() => transform.rotation;
        
        [SerializeField]
        private VehicleInfo _vehicleInfo;
        public void AddVehicle(VehicleInfo vehicle) => _vehicleInfo = vehicle;
        public VehicleInfo GetVehicle() => _vehicleInfo;
        
        public bool IsOccupied => _vehicleInfo != null;
        public EVehicleSize VehicleSize => _sizeType;
    }
}