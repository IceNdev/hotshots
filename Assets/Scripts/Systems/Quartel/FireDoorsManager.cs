﻿using System;
using System.Collections;
using System.Collections.Generic;
using Controllers;
using UnityEngine;
using Utils;

namespace Systems.Quartel
{
    public class FireDoorsManager : Singleton<FireDoorsManager>
    {
        [SerializeField]
        private float _rotationFrontOpen = -87.0f;
        [SerializeField]
        private float _rotationBackOpen = 87.0f;
        [SerializeField]
        private float _rotationClose = 0.0f;
        [SerializeField] private float _timeToRotate = 3f;
        
        [SerializeField]
        private GameObject[] _frontDoors, _backdoors;

        private bool _status, _isOpening;

        private void Start()
        {
            
        }

        public void ToggleDoor()
        {
            Debug.Log("[DOOR]: Try Open");
            Debug.Log($"[DOOR]: Tweens Running : {LeanTween.tweensRunning}");
            if (_isOpening) 
                return;
            Debug.Log("[DOOR]: Can Open");
            _status = !_status;
            StartCoroutine(ToggleDoorsCorountine());
        }

        private IEnumerator ToggleDoorsCorountine()
        {
            LeanTween.reset();
            LeanTween.init(200);
            _isOpening = true;
            ToggleDoors(ref _frontDoors, true);
            yield return new WaitForSeconds(1f);
            ToggleDoors(ref _backdoors, false);
            yield return new WaitForSeconds(1f);

            _isOpening = false;
            yield return null;
        }

        private void ToggleDoors(ref GameObject[] doors, bool isFront)
        {
            for (int i = 0; i < doors.Length; i++)
            {
                if (_status)
                {
                    Vector3 rotation = new Vector3(0.0f, 0.0f, 0.0f);
                    if (isFront)
                    {
                        rotation = new Vector3(0.0f, 0.0f, _rotationFrontOpen);
                        rotation.z = Mathf.Clamp(rotation.z, _rotationFrontOpen, _rotationClose);
                    }
                    else
                    {
                        rotation = new Vector3(0.0f, 0.0f, _rotationBackOpen);
                        rotation.z = Mathf.Clamp(rotation.z, _rotationClose, _rotationBackOpen);
                    }
                    LeanTween.rotateLocal(doors[i], rotation, _timeToRotate).setEaseSpring();
                }
                else
                {
                    LeanTween.rotateLocal(doors[i], new Vector3(0.0f, 0.0f, _rotationClose), _timeToRotate);
                }
            }
        }
    }
}