
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityTemplateProjects;
using Systems.GPS;

public class MissionManager : MonoBehaviour
{
    public bool mission_active;
    public GameObject fi;
    public GameObject[] FireNodes;
    public FireNodeChain[] HouseNodesChain;
    private int rnd;
    private int min;
    public GameObject _cube;
    private GPSPathFinder test1;
    [SerializeField] public float timeRemainingMission = 5;
    [SerializeField] private float timeRemainingResetT = 10;
    public FireManager fr;
    public GameObject FirePrefab;
    public GPSNode start = null;
    [SerializeField] private float _gpsSensorRadius = 80.0f;
    [SerializeField] private LayerMask _gpsSensorLayer;
    public bool housefire;

    private Vector3 _firePos;

    public Vector3 FirePos => _firePos;
    
    void Update()
    {  
        if (timeRemainingMission > 0)
        {
            timeRemainingMission -= Time.deltaTime;
        }
        else
        {   
            if (!mission_active)
            { 
                rnd = Random.Range(0,2);
                if(rnd == 0)
                {
                    mission_active = true;
                    Debug.Log("WildFire");
                    rnd = Random.Range(0,FireNodes.Length);
                    MakeFire(fi, FireNodes[rnd].transform.position);
                    start = GetClosestNode(FireNodes[rnd].transform.position);
                    housefire = false;
                }
                else
                {
                    mission_active = true;
                    Debug.Log("FireHouse");
                    rnd = Random.Range(0,HouseNodesChain.Length);
                    HouseNodesChain[rnd].GetComponent<FireNodeChain>().StartFire();
                    SendFirePositionHouse(HouseNodesChain[rnd].transform.position);
                    start = GetClosestNode(HouseNodesChain[rnd].transform.position);
                    housefire = true;
                }
                
            }
        }

        if (timeRemainingResetT > 0)
        {
            timeRemainingResetT -= Time.deltaTime;
        }
        else
        {
            if(!mission_active){
                fr.TerrainRestore();    
                ReplaceFNC();
                timeRemainingResetT = 10;
            }
        }
    }

    public void SendFirePositionWild(Vector3 pos)
    {
        NotificationManager.Instance.SendNotification("Wild Fire", "Follow the GPS line");
        _firePos = pos;
    }

    public void SendFirePositionHouse(Vector3 pos)
    {
        NotificationManager.Instance.SendNotification("House Fire", "Follow the GPS line");
        _firePos = pos;
    }
    
    void MakeFire(GameObject _fi, Vector3 _firenode) 
    {
        Vector3 rnd_pos = new Vector3 (Random.Range(10, -10), 0, Random.Range(10, -10));
        Instantiate(_fi, _firenode + rnd_pos ,Quaternion.identity);   
    }

    void ReplaceFNC()
    {
        for(int i = 0; i < HouseNodesChain.Length; i++)
        {
            for(int j = 0; j < HouseNodesChain[i].GetComponent<FireNodeChain>().m_fireNodes.Length; j++)
            {
                HouseNodesChain[i].GetComponent<FireNodeChain>().m_fireNodes[j].m_HP = 200.0f;
                HouseNodesChain[i].GetComponent<FireNodeChain>().m_fireNodes[j].m_fuel = 200.0f;
                HouseNodesChain[i].GetComponent<FireNodeChain>().m_fireNodes[j].m_fire = FirePrefab;
                HouseNodesChain[i].GetComponent<FireNodeChain>().m_fireNodes[j].ResetBools();
            }
        }

    }

    private GPSNode GetClosestNode(Vector3 position)
    {
        GPSNode bestNode = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = position;
        RaycastHit[] objectsResult = Physics.SphereCastAll(currentPosition, _gpsSensorRadius, Vector3.forward,
            _gpsSensorRadius, _gpsSensorLayer, QueryTriggerInteraction.Collide);

        if (objectsResult == null)
            return null;
        
        for (int i = 0; i < objectsResult.Length; i++)
        {
            RaycastHit potentialTarget = objectsResult[i];
            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestNode = potentialTarget.collider.gameObject.GetComponent<GPSNode>();
            }
        }

        return bestNode;
    }

    public Transform GetMissionTransform()
    {
        if (start == null) 
            return null;

        return start.transform;
    }
}
