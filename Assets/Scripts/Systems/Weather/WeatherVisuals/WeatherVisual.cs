﻿using UnityEngine;

namespace Systems.Weather.WeatherVisuals
{
    [CreateAssetMenu(fileName = "WeatherVisual", menuName = "WeatherVisuals", order = 0)]
    public class WeatherVisual : ScriptableObject
    {
        public AnimationCurve sunBrightness = new AnimationCurve(
        new Keyframe(0f, 0.02f),
        new Keyframe(0.15f, 0.05f),
        new Keyframe(0.35f, 1f),
        new Keyframe(0.65f, 1f),
        new Keyframe(0.85f, 0.02f),
        new Keyframe(1f, 0.02f)
    );

    public Gradient sunColor = new Gradient()
    {
        colorKeys = new GradientColorKey[2]
        {
            new GradientColorKey(new Color(1f, 0.75f, 0.3f), 0.25f),
            new GradientColorKey(new Color(0.95f, 0.95f, 1f), 0.50f)
        },
        alphaKeys = new GradientAlphaKey[2]
        {
            new GradientAlphaKey(1.0f, 0.0f),
            new GradientAlphaKey(1.0f, 1.0f)
        }
    };

    [GradientUsage(true)] public Gradient skyColorDay = new Gradient()
    {
        colorKeys = new GradientColorKey[3]
        {
            new GradientColorKey(new Color(0.75f, 0.3f, 0.17f), 0.0f),
            new GradientColorKey(new Color(0.7f, 1.4f, 3), 0.5f),
            new GradientColorKey(new Color(0.75f, 0.3f, 0.17f), 1.0f)
        },
        alphaKeys = new GradientAlphaKey[2]
        {
            new GradientAlphaKey(1.0f, 0.0f),
            new GradientAlphaKey(1.0f, 1.0f)
        }
    };

    [GradientUsage(true)] public Gradient skyColorNight = new Gradient()
    {
        colorKeys = new GradientColorKey[3]
        {
            new GradientColorKey(new Color(0.75f, 0.3f, 0.17f), 0.0f),
            new GradientColorKey(new Color(0.44f, 1, 1), 0.5f),
            new GradientColorKey(new Color(0.75f, 0.3f, 0.17f), 1.0f)
        },
        alphaKeys = new GradientAlphaKey[2]
        {
            new GradientAlphaKey(1.0f, 0.0f),
            new GradientAlphaKey(1.0f, 1.0f)
        }
    };

    public AnimationCurve skyIntensity = new AnimationCurve(
        new Keyframe(0f, 0.02f),
        new Keyframe(0.15f, 0.05f),
        new Keyframe(0.35f, 1f),
        new Keyframe(0.65f, 1f),
        new Keyframe(0.85f, 0.02f),
        new Keyframe(1f, 0.02f)
    );

    public AnimationCurve skyFogDensity = new AnimationCurve(
        new Keyframe(0f, 0.0f),
        new Keyframe(0.30f, 0.0f),
        new Keyframe(0.35f, 0.01f),
        new Keyframe(0.65f, 0.01f),
        new Keyframe(0.70f, 0.0f),
        new Keyframe(1f, 0.0f)
    );

    public Gradient fogColor = new Gradient()
    {
        colorKeys = new GradientColorKey[5]
        {
            new GradientColorKey(new Color(0.83f, 0.9f, 0.9f), 0f),
            new GradientColorKey(new Color(0.83f, 0.9f, 0.9f), 0.15f),
            new GradientColorKey(new Color(1, 0.54f, 0.37f), 0.25f),
            new GradientColorKey(new Color(0.95f, 0.95f, 1), 0.5f),
            new GradientColorKey(new Color(1.0f, 1.0f, 1.0f), 1f),
        },
        alphaKeys = new GradientAlphaKey[2]
        {
            new GradientAlphaKey(1, 0),
            new GradientAlphaKey(1, 1)
        }
    };
    }
}
