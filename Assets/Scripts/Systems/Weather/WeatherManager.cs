﻿using System;
using System.Collections;
using System.Collections.Generic;
using Systems.Weather.WeatherVisuals;
using DayNight;
using UnityEngine;
using Utils;
using Random = System.Random;

namespace Systems.Weather
{
    public enum WeatherState
    {
        Sunny,
        Cloudy,
        Rainy
    }
    public class WeatherCondition
    {
        public WeatherState WeatherState;
        public Material SkyboxMaterial;
        public WeatherVisual Visual;
        public float Wind;
        public bool Rain;
        public bool NormalSun;
    }
    public class WeatherManager : Singleton<WeatherManager>
    {
        
        [SerializeField]
        private Material _normalSkybox, _cloudRainSkybox;

        [SerializeField] private WeatherVisual _sunnyVisual, cloudyVisual;

        [SerializeField] private GameObject _rainParticleSystem, _sun;

        private Light _sunLight, _sunLightRain;
        private DayNightController _sunDNC, _sunDNCRain;
        
        // Hidden Markov Model = hMM
        private Dictionary<WeatherState, List<Tuple<WeatherState, float>>> _hMM;
        private List<WeatherCondition> _weatherConditions;

        private WeatherState _currentState;
        
        void Start()
        {
            _hMM = new Dictionary<WeatherState, List<Tuple<WeatherState, float>>>();
            _weatherConditions = new List<WeatherCondition>()
            {
                new WeatherCondition
                {
                    WeatherState = WeatherState.Sunny,
                    SkyboxMaterial = _normalSkybox,
                    Visual = _sunnyVisual,
                    Wind = 0.1f,
                    Rain = false,
                    NormalSun = true
                },
                new WeatherCondition
                {
                    WeatherState = WeatherState.Cloudy,
                    SkyboxMaterial = _cloudRainSkybox,
                    Visual = cloudyVisual,
                    Wind = 0.5f,
                    Rain = false,
                    NormalSun = false
                },
                new WeatherCondition
                {
                    WeatherState = WeatherState.Rainy,
                    SkyboxMaterial = _cloudRainSkybox,
                    Visual = cloudyVisual,
                    Wind = 1.0f,
                    Rain = true,
                    NormalSun = false
                }
            };
            _currentState = WeatherState.Sunny;
            
            _sunLight = _sun.GetComponent<Light>();
            _sunDNC = _sun.GetComponent<DayNightController>();
            
            AddWeather(WeatherState.Sunny, WeatherState.Sunny, 0.8f);
            AddWeather(WeatherState.Sunny, WeatherState.Cloudy, 0.2f);
            
            AddWeather(WeatherState.Cloudy, WeatherState.Cloudy, 0.2f);
            AddWeather(WeatherState.Cloudy, WeatherState.Rainy, 0.2f);
            AddWeather(WeatherState.Cloudy, WeatherState.Sunny, 0.6f);
            
            AddWeather(WeatherState.Rainy, WeatherState.Rainy, 0.2f);
            AddWeather(WeatherState.Rainy, WeatherState.Cloudy, 0.8f);

            string debugOutput = String.Empty;
            
            foreach (var items in _hMM.Values)
            {
                foreach (var item in items)
                {
                    debugOutput += $"{item.Item1} [{item.Item2}] \n";
                }
            }
        }
        
        private void AddWeather(WeatherState from, WeatherState to, float probability)
        {
            if(!_hMM.ContainsKey(from))
                _hMM.Add(from, new List<Tuple<WeatherState, float>>());
            _hMM[from].Add(new Tuple<WeatherState, float>(to, probability));
        }

        public void ChangeWeather()
        {
            _currentState = GetNextWeatherState().Value;
            ExecuteWeatherCondition();
            Debug.Log(GetCurrentWeatherState());
        }

        public string GetCurrentWeatherState() => _currentState.ToString();

        private WeatherState? GetNextWeatherState()
        {
            Random rnd = new Random();
            float random = (float)rnd.NextDouble();
            float sum = 0.0f;
            foreach (var item in _hMM[_currentState])
            {
                float prob = item.Item2;
                if (sum <= random && random < (sum + prob))
                    return item.Item1;
                sum += prob;
            }
            return null;
        }

        private void ExecuteWeatherCondition()
        {
            WeatherCondition weatherCondition = GetWeatherCondition(_currentState);
            RenderSettings.skybox = weatherCondition.SkyboxMaterial;
            _rainParticleSystem.SetActive(weatherCondition.Rain);
            // We need to control this, to make sure we don't have 2 light sources in the scene
            // So if we want to enable normal sun, first we need to disable 'sunRain', so then enabling 'sun' will not be a problem
            /*if (weatherCondition.NormalSun)
            {
                _sunLightRain.enabled = !weatherCondition.NormalSun;
                _sunLight.enabled = weatherCondition.NormalSun;
            }
            else
            {
                _sunLight.enabled = weatherCondition.NormalSun;
                _sunLightRain.enabled = !weatherCondition.NormalSun;
            }*/
            
        }
        
        public WeatherCondition GetWeatherCondition(WeatherState wantedStated)
        {
            return _weatherConditions.Find(x => x.WeatherState == wantedStated);
        }
        
        public WeatherCondition GetWeatherCondition()
        {
            return _weatherConditions.Find(x => x.WeatherState == _currentState);
        }
    }
}
