﻿using System.Collections;
using Systems.Quartel;
using UnityEngine;

namespace Systems
{
    public enum WaterPumpMode
    {
        Refill,
        Empty
    }
    public class WaterPump
    {
        public WaterTank tankInput, tankOutput;
        public WaterPumpMode Mode = WaterPumpMode.Refill;
        [SerializeField]
        private float _pumpRate = 1.5f;

        public bool Work()
        {
            if (Mode == WaterPumpMode.Refill)
            {
                if (tankOutput == null || tankInput == null) 
                    return false;
                float waterToLoad = tankOutput.WaterLoaded;
                if (waterToLoad > tankInput.FreeCapacity())
                {
                    waterToLoad -= (waterToLoad - tankInput.FreeCapacity());
                }
                
                tankInput.AddWater(waterToLoad * _pumpRate * _pumpRate);
                tankOutput.RemoveWater(waterToLoad * _pumpRate * _pumpRate);

                return true;
            }
            else
            {
                // Doesn't let the pump work, because it needs to water tanks to work.
                if (tankInput == null || tankOutput == null) 
                    return false;
                
                // Gets the amount of water that needs to be transferred 
                float waterToEmpty = tankInput.WaterLoaded;
                
                // Make sure that the water tank target has enough capacity
                if (waterToEmpty > tankOutput.FreeCapacity())
                {
                    // Calculates the specific amount that can be filled to the water tank target
                    waterToEmpty -= waterToEmpty - tankOutput.FreeCapacity();
                }
                
                tankOutput.AddWater(waterToEmpty);
                tankInput.RemoveWater(waterToEmpty);
                
                return true;
            }

            return false;
        }

        public void ToggleWorkMode() => Mode = Mode == WaterPumpMode.Refill ? WaterPumpMode.Empty : WaterPumpMode.Refill;
        
    }

}