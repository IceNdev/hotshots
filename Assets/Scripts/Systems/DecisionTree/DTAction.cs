﻿using System;

namespace Systems.DecisionTree
{
    public class DTAction : DTNode
    {
        private Action _action;

        public DTAction(Action action)
        {
            _action = action;
        }

        public override void Run()
        {
            _action();
        }
    }
}