﻿using System;

namespace Systems.DecisionTree
{
    public class DTCondition : DTNode
    {
        private DTNode _trueChild, _falseChild;
        private Func<bool> _condition;

        public DTCondition(Func<bool> condition, DTNode tChild, DTNode fChild)
        {
            _condition  = condition;
            _trueChild  = tChild;
            _falseChild = fChild;
        }

        public override void Run()
        {
            if (_condition()) _trueChild.Run();
            else _falseChild.Run();
        }
    }
}