﻿namespace Systems.DecisionTree
{
    public abstract class DTNode
    {
        public abstract void Run();
    }
}