﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class StreetLightsManager : Singleton<StreetLightsManager>
{
    [SerializeField]
    private Material[] _lightMaterial;

    [SerializeField] 
    private Light[] _spotLights;

    [SerializeField] 
    private GameObject _streetLights;

    public void Start()
    {
        _spotLights = _streetLights.GetComponentsInChildren<Light>();
    }

    public void SetLightActive(bool status)
    {
        if (status == true)
        {
            for (int i = 0; i < _lightMaterial.Length; i++)
                _lightMaterial[i].EnableKeyword("_EMISSION");
        }
        else
        {
            for (int i = 0; i < _lightMaterial.Length; i++)
                _lightMaterial[i].DisableKeyword("_EMISSION");
        }
        /*shop
        if (status == true)
            _lightMaterial.EnableKeyword("_EMISSION");
        else
            _lightMaterial.DisableKeyword("_EMISSION");*/
        
        foreach (Light light in _spotLights)
        {
            light.enabled = status;
        }
    }
}
