﻿using System.Collections;
using System.Collections.Generic;
using Controllers;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    [RequireComponent(typeof(HoseController))]
    public class WaterHoseEffectManager : MonoBehaviour
    {
        public ParticleSystem ps;
        private PlayerInActions _controls;
        private float add = 0f;
        private float mScroll;
        private bool emb;
        ParticleSystem.MainModule  main;
        
        // Hose System
        [SerializeField]
        private HoseController _hoseController;

        [SerializeField] private float _waterRemoveSpeed = 100.0f;

        #region  Enable/Disable
        private void OnEnable()
        {
            _controls?.Player.HosePower.Enable();
        }
        private void OnDisable()
        {
            _controls?.Player.HosePower.Disable();
        }
        #endregion

        private void Awake()
        {
            _controls = PlayerInputs.Instance.PlayerControls;        
            _controls.Player.HosePower.performed += x => mScroll = x.ReadValue<float>();
            main = ps.main;
        }

        // Update is called once per frame
        void Update()
        {
            if (_hoseController.Hose == null || _hoseController.Hose.WaterTank1 == null) 
                return; 
            
            if (mScroll > 0f && main.startLifetime.constant < main.startSpeed.constant) // forward
            {  
                add += 0.1f;
                main.startLifetime = add;  
                    
            }
            else if (mScroll < 0f) // backwards
            {
                add -= 0.1f;
                main.startLifetime = add;
            }
            if (main.startLifetime.constant < 0.1f) 
            {
                emb = false;
                var emission = ps.emission;
                emission.enabled = emb;
            }
            else {
                emb = true;
                var emission = ps.emission;
                emission.enabled = emb;
            }
            if(add > 0.0f)
                _hoseController.Hose.WaterTank1.RemoveWater(add * _waterRemoveSpeed * Time.deltaTime);
        }
    }
}
