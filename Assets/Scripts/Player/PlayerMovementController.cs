﻿using System;
using Controllers;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerMovementController : MonoBehaviour
    {
        public float defaultSpeed = 5f;
        public float gravity = -9.81f;
        public float jumpHeight = 3f;

        public Transform groundCheck;
        public float groundDistance = 0.5f;
        public LayerMask groundMask;

        private float _speed;
        private float _sprintSpeed;
        private Vector3 _velocity;
        private bool _isGrounded;
        
        // Locks the movement when it's true
        private bool _lockMovement;
        
        private PlayerInActions _controls;
        private CharacterController _controller;
        private PlayerCameraController _playerCameraController;
        private PlayerInput _playerInput;
        private HoseController _hoseController;
        
        public void SetLock(bool status) => _lockMovement = status;
        
        private void Awake()
        {
            _controls = PlayerInputs.Instance.PlayerControls;
            _controls.Player.Jump.performed += JumpOnperformed;
        }

        private void Start()
        {
            _controller             = GetComponent<CharacterController>();
            _playerCameraController = GetComponent<PlayerCameraController>();
            _playerInput            = GetComponent<PlayerInput>();
            _hoseController         = GetComponent<HoseController>();
            
            _hoseController.UpdatePlayerPosition(_controller.transform.position);
            _hoseController.Initialize();
        }
     
        private void OnEnable()
        {
            _controls?.Player.Move.Enable();
            _controls?.Player.Jump.Enable();
            _controls?.Player.HoseConnector.Enable();

            if (_controller != null)
            {
                _controller.enabled = true;
                _playerCameraController.enabled = true;
                _playerInput.enabled = true;
            }
        }

        private void OnDisable()
        {
            _controls?.Player.Move.Disable();
            _controls?.Player.Jump.Disable();
            _controls?.Player.HoseConnector.Disable();

            _controller.enabled = false;
            _playerCameraController.enabled = false;
            _playerInput.enabled = false;
            
        }

        private void Update()
        {
            if (_lockMovement) return;
            
            _isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
            if (_isGrounded && _velocity.y < 0.0f)
                _velocity.y = 0f;

            Move();
        }

        private void Move()
        {
            Vector2 movementInput = _controls.Player.Move.ReadValue<Vector2>();
            Vector3 move = transform.right * movementInput.x + transform.forward * movementInput.y;

            _speed = defaultSpeed;
            _velocity.y += gravity * Time.deltaTime;
            
            _controller.Move((move + _velocity) * _speed * Time.deltaTime);
            
            if(movementInput.magnitude > 0.0f)
                _hoseController.UpdatePlayerPosition(_controller.transform.position);
        }
        
        private void JumpOnperformed(InputAction.CallbackContext callback)
        {
            if (_isGrounded)
                _velocity.y = Mathf.Sqrt(jumpHeight * -2.0f * gravity);
        }
    }
}