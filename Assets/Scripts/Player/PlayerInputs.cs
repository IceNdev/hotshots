﻿using System;
using UnityEngine;
using Utils;

namespace Player
{
    public class PlayerInputs : Singleton<PlayerInputs>
    {
        
        private PlayerInActions _controls;
        
        public PlayerInActions PlayerControls
        {
            get
            {
                if (_controls != null) return _controls;
                return _controls = new PlayerInActions();
            }
        }
    }
}