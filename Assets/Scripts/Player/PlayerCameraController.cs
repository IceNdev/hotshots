﻿using System;
using UnityEngine;

namespace Player
{
    public class PlayerCameraController : MonoBehaviour
    {
        public float mouseSensitivity = 8.0f;
        public Transform playerBody;
        public Camera fpsCamera;

        private float _rotationX;
        
        // Locks the camera rotation when it's true
        private bool _lockRotation;

        private PlayerInActions _controls;
        public void SetLock(bool status) => _lockRotation = status;
        
        private void Awake()
        {
            _controls = PlayerInputs.Instance.PlayerControls;
            
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void OnEnable()
        {
            _controls.Player.Look.Enable();
        }

        private void OnDisable()
        {
            _controls.Player.Look.Disable();
        }

        private void Update()
        {
            if (_lockRotation) return;
            RotateCamera();
        }

        private void RotateCamera()
        {
            Vector2 movementInput = _controls.Player.Look.ReadValue<Vector2>();

            movementInput.x *= mouseSensitivity;
            movementInput.y *= mouseSensitivity;

            // Pitch
            _rotationX -= movementInput.y;
            _rotationX = Mathf.Clamp(_rotationX, -90.0f, 90.0f);
            fpsCamera.transform.localRotation = Quaternion.Euler(_rotationX, 0.0f, 0.0f);
            // Yaw
            playerBody.Rotate(Vector3.up * movementInput.x);
        }
    }
}