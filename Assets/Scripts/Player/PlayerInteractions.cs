﻿using System;
using Systems.Quartel;
using Controllers;
using UI;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerInteractions : MonoBehaviour
    {
        private PlayerCameraController _cameraController;
        private PlayerMovementController _movementController;
        private HoseController _hoseController;
        private PlayerInActions _controls;
        private Transform _cameraTransform;
        private bool _uiLock;

        //Hotbar stuff
        [SerializeField] private HotbarManager _hotBar;
        private bool[] slotsOccupied = {false, false, false, false, false};
        private Item item;
        
        private void Awake()
        {
            _controls = PlayerInputs.Instance.PlayerControls;
            _controls.Player.Interaction.performed += InteractionOnperformed;
            _controls.Player.HoseConnector.performed += HoseConnectorOnperformed;
        }
        
        private void Start()
        {
            _cameraController   = GetComponent<PlayerCameraController>();
            _movementController = GetComponent<PlayerMovementController>();
            _hoseController     = GetComponent<HoseController>();
        }

        private void InteractionOnperformed(InputAction.CallbackContext obj)
        {
            _cameraTransform    = _cameraController.fpsCamera.transform;
            RaycastHit hit;
            if (Physics.Raycast(_cameraTransform.position, _cameraTransform.forward, out hit, 2.0f))
            {
                Debug.Log($"Hit: {hit.collider.name}");
                if (hit.collider.CompareTag("QuartelComputer"))
                {
                    _uiLock = !_uiLock;
                    _cameraController.SetLock(_uiLock);
                    _movementController.SetLock(_uiLock);
                    QuartelManager.Instance.SetUIStatus(_uiLock);
                }
                /*else if (hit.collider.CompareTag("Hose"))
                {
                    _hoseController.Hose = hit.collider.gameObject.GetComponent<Hose>();
                    _hoseController.Hose.Reset();
                }*/
                else if (hit.collider.CompareTag("PickupObject"))
                {
                    item = hit.transform.gameObject.GetComponent<Item>();
                    
                    for (int i = 0; i < slotsOccupied.Length; i++)
                    {
                        if (!slotsOccupied[i])
                        {
                            _hotBar.SetImage(i, item.Name);
                            slotsOccupied[i] = true;
                            break;
                        }
                    }
                }
            }
        }
        
        private void HoseConnectorOnperformed(InputAction.CallbackContext obj)
        {
            _cameraTransform = _cameraController.fpsCamera.transform;
            RaycastHit hit;
            if (Physics.Raycast(_cameraTransform.position, _cameraTransform.forward, out hit, 2.0f))
            {
                string hitTag = hit.collider.tag;
                if (hitTag == "Truck" || hitTag == "WaterTank")
                {
                    GameObject hitObject = hit.collider.gameObject;
                    foreach (Transform child in hitObject.transform)
                    {
                        if (child.CompareTag("HoseConnector") && _hoseController.Hose != null)
                        {
                            WaterTank waterTank = hitObject.GetComponent<WaterTank>();
                            bool hoseActive = !_hoseController.Hose.Active();
                            
                            // If it's not active, then it means the hose has no positions, so it sets the first position
                            // When it's active it means is trying to connect to another hose connector.
                            _hoseController.Hose.AddHoseConnectors(hoseActive, child.position);
                            if (hoseActive)
                            {
                                _hoseController.Hose.WaterTank1 = waterTank;
                            }
                            else
                            {
                                _hoseController.Hose.WaterTank2 = waterTank;
                                _hoseController.Hose = null;
                            }
                            waterTank.Hose = _hoseController.Hose;   
                        }
                    }
                }
                else if (hitTag == "Hose")
                {
                    _hoseController.Hose = hit.collider.gameObject.GetComponent<Hose>();
                    _hoseController.Hose.Reset();
                }
            }
        }
    }
}