﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using Utils;

public class EconomyManager : Singleton<EconomyManager>
{
    [Header("Paycheck")]
    [SerializeField] private float _payCheck = 150.0f;
    [Header("Balance")]
    [SerializeField] private float _money = 1000000;

    private bool _started;
    
    public float Money
    {
        get => _money;
        set => _money = value;
    }
    public bool DeductMoney(float cost)
    {
        if (cost <= _money)
        {
            _money -= cost;
            return true;
        }
        return false;
    }

    public void AddMoney(float income) => _money += income;

    public bool CanPay(float cost) => cost <= _money;

    public void Paycheck()
    {
        if(_started)
            AddMoney(_payCheck);
    }

    public void Init() => _started = true;

    private void OnDestroy()
    {
        Destroy(this);
    }
}
