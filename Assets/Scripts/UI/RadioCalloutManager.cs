﻿using System;
using System.Collections;
using System.Collections.Generic;
using Systems;
using UnityEngine;
using Player;
using TMPro;
using UnityEngine.InputSystem;

public class RadioCalloutManager : MonoBehaviour
{
    private PlayerInActions inputs;
    
    void Awake() => inputs = PlayerInputs.Instance.PlayerControls;

    private void OnEnable()
    {
        inputs.UIRadio.Key1.performed += ActionKey1;
        inputs.UIRadio.Key2.performed += ActionKey2;
        inputs.UIRadio.Key3.performed += ActionKey3;
        inputs.UIRadio.Enable();
    }
    
    private void OnDisable()
    {
        inputs.UIRadio.Key1.performed -= ActionKey1;
        inputs.UIRadio.Key2.performed -= ActionKey2;
        inputs.UIRadio.Key3.performed -= ActionKey3;
        //inputs.UIRadio.Disable();
    }

    private void ActionKey1(InputAction.CallbackContext obj)
    {
        AIManager.Instance.EnterVehicle();
    }
    
    private void ActionKey2(InputAction.CallbackContext obj)
    {
        AIManager.Instance.ExitVehicle();
    }
    
    private void ActionKey3(InputAction.CallbackContext obj)
    {
        AIManager.Instance.CombatFire();
    }
}
