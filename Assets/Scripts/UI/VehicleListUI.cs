﻿using System;
using System.Collections.Generic;
using Systems.Quartel;
using TMPro;
using UnityEngine;
using Vehicle;

namespace UI
{
    public class VehicleListUI : MonoBehaviour
    {
        [SerializeField] private GameObject textTemplate;
        private Vector3 _spawnPosition;
        
        private void Start()
        {
            _spawnPosition = textTemplate.GetComponent<RectTransform>().anchoredPosition;
            UpdateScrollView();
        }

        private void OnEnable()
        {
            
        }

        private void Update()
        {
            bool isUiActive = QuartelManager.Instance.IsUIActive();
            if (isUiActive)
            {
                UpdateScrollView();
            }
        }

        private void UpdateScrollView()
        {
            List<VehicleInfo> vehicles = QuartelManager.Instance.GetVehicles();
            for (int i = 0; i < vehicles.Count; i++)
            {
                string vehicleName = vehicles[i].GetName();
                float spawnY = _spawnPosition.y - (i * 60);

                Vector3 position = new Vector3(_spawnPosition.x, spawnY, _spawnPosition.z);
                
                GameObject textObject = Instantiate(textTemplate, position, textTemplate.transform.rotation) as GameObject;
                textObject.SetActive(true);
                textObject.GetComponent<TextMeshProUGUI>().SetText(vehicleName);
                
                textObject.transform.SetParent(textTemplate.transform.parent, false);
            }
        }
    }
}