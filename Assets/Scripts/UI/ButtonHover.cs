﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonHover : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] private float _scaleHover = 0.1f;
    
    private RectTransform _rectTransform;
    private Vector3 _hoverScale, _normalScale;
    private bool _isHovering = false;
    
    void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _hoverScale = new Vector3(_scaleHover, _scaleHover, _scaleHover) + _rectTransform.localScale;
        _normalScale = _rectTransform.localScale;
    }


    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        LeanTween.init(200);
        LeanTween.scale(_rectTransform, _hoverScale, 0.2f).setEase(LeanTweenType.linear).setOnComplete(OnExit);
    }

    private void OnExit()
    {
        LeanTween.scale(_rectTransform, _normalScale, 0.2f).setEase(LeanTweenType.linear);
    }
}
