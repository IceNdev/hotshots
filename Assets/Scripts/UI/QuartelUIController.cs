﻿using System;
using Systems.Quartel;
using Player;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;

namespace UI
{
    public class QuartelUIController : Singleton<QuartelUIController>
    {
        [SerializeField] private TextMeshProUGUI _moneyText;

        private void Start()
        {
            UpdateUI();   
        }

        private void Update()
        {
            UpdateUI();
        }

        private void UpdateUI()
        {
            _moneyText.SetText(EconomyManager.Instance.Money.ToString("0"));
        }
    }
}