﻿using System;
using UnityEngine;

namespace UI
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField]
        private int _progressBarMaxValue = 134;
        
        [SerializeField]
        private RectTransform _progressBarImg;

        private void Start()
        {
            _progressBarImg = GetComponent<RectTransform>();
        }
        // Value goes from 0 to 1
        public void SetProgressBar(float value)
        {
            if(_progressBarImg == null) return;
            int width = Convert.ToInt32(_progressBarMaxValue * value);
            _progressBarImg.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        }
    }
}