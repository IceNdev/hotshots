﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private Dropdown resolutionsDropdown;
    [SerializeField] private Dropdown qualityDropdown;
    [SerializeField] private Slider overallVolume;
    [SerializeField] private Slider musicVolume;
    [SerializeField] private Slider vehicleVolume;
    
    [SerializeField] private Dropdown mmRes = null;
    [SerializeField] private Dropdown mmQual = null;
    [SerializeField] private Slider mmVol = null;
    [SerializeField] private Slider mmMusic = null;
    [SerializeField] private Slider mmVehicleVol = null;

    private float _overallVolumeValue;
    private float _musicVolumeValue;
    private float _vehicleVolumeValue;
    
    private Resolution[] _resolutions;
    
    void Start()
    {
        qualityDropdown.value = qualityDropdown.options.IndexOf(qualityDropdown.options.Last());
        _resolutions = Screen.resolutions;
        
        resolutionsDropdown.ClearOptions();

        List<string> resolutionList = new List<string>();

        for (int i = 0; i < _resolutions.Length; i++)
        {
            string option = _resolutions[i].width + " x " + _resolutions[i].height;
            resolutionList.Add(option);
        }
        
        resolutionsDropdown.AddOptions(resolutionList);
        resolutionsDropdown.value = _resolutions.Length - 1;
        resolutionsDropdown.RefreshShownValue();
    }

    private void OnEnable()
    {
        if (mmRes != null && mmQual != null && mmVol != null && mmMusic != null && mmVehicleVol != null)
        {
            resolutionsDropdown.value = mmRes.value;
            qualityDropdown.value = mmQual.value;
            overallVolume.value = mmVol.value;
            musicVolume.value = mmMusic.value;
            vehicleVolume.value = mmVehicleVol.value;
        }
    }

    public void SaveVolumes()
    {
        _overallVolumeValue = overallVolume.value;
        _musicVolumeValue = musicVolume.value;
        _vehicleVolumeValue = vehicleVolume.value;
    }

    public void SetVolumes()
    {
        SaveVolumes();
        
        _audioMixer.SetFloat("OverallVolume", Mathf.Log10(_overallVolumeValue) * 20);
        _audioMixer.SetFloat("MusicVolume", Mathf.Log10(_musicVolumeValue) * 20);
        _audioMixer.SetFloat("VehicleVolume", Mathf.Log10(_vehicleVolumeValue) * 20);
    }

    public void SetOverallVolume(Single slider)
    {
        _audioMixer.SetFloat("OverallVolume", Mathf.Log10(slider) * 20);
    }

    /*
    public void SetOverallVolume(float volume)
    {
        _audioMixer.SetFloat("OverallVolume", volume);
    }

    public void SetMusicVolume(float volume) => _audioMixer.SetFloat("MusicVolume", volume);
    
    public void SetVehicleVolume(float volume) => _audioMixer.SetFloat("VehicleVolume", volume);
    */

    public void SetGraphicsQuality(int index) => QualitySettings.SetQualityLevel(index);

    public void SetFullscreen(bool fullscreen) => Screen.fullScreen = fullscreen;

    public void SetResolution(int index) => Screen.SetResolution(_resolutions[index].width, _resolutions[index].height, Screen.fullScreen);
}
