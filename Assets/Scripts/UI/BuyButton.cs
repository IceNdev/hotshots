﻿using System.Collections;
using System.Collections.Generic;
using Systems.Quartel;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityTemplateProjects;
using SaveData;

public class BuyButton : MonoBehaviour
{
    [SerializeField] private Button _button;
    [SerializeField] private string _name;
    [SerializeField] private float _cost;
    [SerializeField] private TextMeshProUGUI _nameText;
    [SerializeField] private TextMeshProUGUI _priceText;
    [SerializeField] private GameObject _prefab;

    void Start()
    {
        _nameText.SetText(_name);
        _priceText.SetText(_cost.ToString() + "€");
        //button = GetComponent<Button>();
    }
    
    // Update is called once per frame
    public void BuyItem()
    {
        if (_prefab.tag.Contains("Truck"))
        {
            if(!CanBuy()) 
                return;
            if (!QuartelManager.Instance.AddCarToGarage(_prefab))
            {
                Debug.Log("Can't add vehicle");
                NotificationManager.Instance.SendNotification("Shop", "Garage slots not available");
            }
            else
            {
                NotificationManager.Instance.SendNotification("Shop", "Vehicle bought");
            }
        }
        else if (_prefab.tag.Contains("FirefighterNPC"))
        {
            if (EconomyManager.Instance.CanPay(_cost))
            {
                switch (QuartelManager.Instance.AddFirefighter(_prefab))
                {
                    case EBuyFireResult.Beds:
                        Debug.Log("Can't recruit firefighter");
                        NotificationManager.Instance.SendNotification("Shop", "You don't have any beds available");
                        break;
                    case EBuyFireResult.Vehicles:
                        Debug.Log("You need more vehicles");
                        NotificationManager.Instance.SendNotification("Shop", "You need more vehicles");
                        break;
                    case EBuyFireResult.Success:
                        EconomyManager.Instance.DeductMoney(_cost);
                        NotificationManager.Instance.SendNotification("Shop", "New firefighter recruited");
                        break;
                }
            }
            else
            {
                NotificationManager.Instance.SendNotification("Shop", "Insufficient money");
            }
        }
    }

    private bool CanBuy()
    {
        if (!EconomyManager.Instance.DeductMoney(_cost))
        {
            // Not enough money
            NotificationManager.Instance.SendNotification("Shop", "Insufficient money");
            return false;
        }

        return true;
    }
}
