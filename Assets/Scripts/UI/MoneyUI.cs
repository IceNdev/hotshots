﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;
    
    // Update is called once per frame
    void Update()
    {
        text.SetText(EconomyManager.Instance.Money.ToString("0") + "€");
    }
}
