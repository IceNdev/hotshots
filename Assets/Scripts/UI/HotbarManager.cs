﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HotbarManager : MonoBehaviour
    {
        [SerializeField] private GameObject[] slots;
        public Sprite hotbar_axe, hotbar_hose;

        private GameObject objectActive;

        public void SetImage(int index, string itemName)
        {
            if (string.Equals(itemName, "Axe"))
            {
                slots[index].GetComponent<Image>().sprite = hotbar_axe;
            }
            else if (string.Equals(itemName, "Hose"))
            {
                slots[index].GetComponent<Image>().sprite = hotbar_hose;
            }
        }
    }
}
