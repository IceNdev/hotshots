﻿using System;
using Player;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private GameObject _uiCamera, _playerCamera;

        // Panels
        [SerializeField] 
        private GameObject _mainPanel, _loadPanel, _optionsPanel, _creditsPanel, _hotbar, _controlsPanel;
        [SerializeField]
        private GameObject _pauseMenu, _pauseMenuOptions;
        [SerializeField] 
        private TextMeshProUGUI _textMoney;
        
        private PlayerInActions inputs;
        public GameObject buyMenu;
        public GameObject radioMenu, radio1, missionManager;
        private bool menuBool;
        private bool radio1bool;

        private GameObject _playerObject;

        void Awake()
        {
            inputs = PlayerInputs.Instance.PlayerControls;
            inputs.UI.BuyMenu.performed += BuyMenuBool;
            inputs.UIRadio.RadioCallouts1.performed += RadioCallouts1Bool;
            inputs.UI.PauseMenu.performed += PauseMenuOnperformed;
        
            buyMenu.SetActive(false);
            radio1.SetActive(false);
            missionManager.SetActive(false);
            _hotbar.SetActive(false);
        }
        
        private void Start()
        {
            SetCursorStatus(true);
            LockPlayer(true);
        }

        private void OnEnable()
        {
            inputs.UI.Enable();
            inputs.UIRadio.RadioCallouts1.Enable();
            inputs.UIRadio.RadioCallouts2.Enable();
        }

        private void OnDisable()
        {
            inputs.UI.Disable();
            inputs.UIRadio.RadioCallouts1.Disable();
            inputs.UIRadio.RadioCallouts2.Disable();
        }

        private void Update()
        {
            if(buyMenu.activeSelf)
                _textMoney.SetText(EconomyManager.Instance.Money.ToString("0") + "€");
        }

        private void BuyMenuBool(InputAction.CallbackContext obj)
        {
            if (_mainPanel.activeSelf) return;
            menuBool = !menuBool;
            LockPlayer(menuBool);
            
            if (menuBool)
                Cursor.lockState = CursorLockMode.None;
            else
                Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = menuBool;
            
            radioMenu.SetActive(false);
            buyMenu.SetActive(menuBool);
            
        }

        private void RadioCallouts1Bool(InputAction.CallbackContext obj)
        {
            if (_mainPanel.activeSelf) return;
            if (!menuBool)
            {
                radioMenu.SetActive(true);

                radio1bool = !radio1bool;
                radio1.SetActive(radio1bool);
            }
        }

        private void PauseMenuOnperformed(InputAction.CallbackContext obj)
        {
            if (_mainPanel.activeSelf) return;
            _pauseMenuOptions.SetActive(false);
            if (_pauseMenu.activeSelf)
            {
                Resume();
                return;
            }
            
            LockPlayer(true);
            _pauseMenu.SetActive(true);
            SetCursorStatus(true);
        }

        public void PlayBtn()
        {
            _uiCamera.SetActive(false);
            _playerCamera.SetActive(true);
            _mainPanel.SetActive(false);
            _hotbar.SetActive(true);
            missionManager.SetActive(true);
            
            LockPlayer(false);
            SetCursorStatus(false);
            EconomyManager.Instance.Init();
        }

        public void LoadBtn()
        {
            _optionsPanel.SetActive(false);
            _creditsPanel.SetActive(false);
            _controlsPanel.SetActive(false);
            _loadPanel.SetActive(true);
            missionManager.SetActive(true);
        }

        public void OptionsBtn()
        {
            _creditsPanel.SetActive(false);
            _loadPanel.SetActive(false);
            _controlsPanel.SetActive(false);
            _optionsPanel.SetActive(true);
        }

        public void OptionsPauseMenuBtn()
        {
            _pauseMenu.SetActive(false);
            _pauseMenuOptions.SetActive(true);
        }
        
        public void OptionsPauseMenuSaveBtn()
        {
            _pauseMenuOptions.SetActive(false);
            _pauseMenu.SetActive(true);
        }

        public void CreditsBtn()
        {
            _optionsPanel.SetActive(false);
            _loadPanel.SetActive(false);
            _controlsPanel.SetActive(false);
            _creditsPanel.SetActive(true);
        }
        
        public void ControlsBtn()
        {
            _optionsPanel.SetActive(false);
            _loadPanel.SetActive(false);
            _creditsPanel.SetActive(false);
            _controlsPanel.SetActive(true);
        }
        
        public void QuitBtn()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }


        public void Resume()
        {
            Debug.Log("resume");
            _pauseMenu.SetActive(false);
            LockPlayer(false);
            SetCursorStatus(false);
        }

        private void LockPlayer(bool status)
        {
            // This might be a issue, because of "FindGameObjectWithTag"
            if(_playerObject == null)
                _playerObject = GameObject.FindGameObjectWithTag("Player");
            
            _playerObject.GetComponent<PlayerMovementController>().SetLock(status);
            _playerObject.GetComponent<PlayerCameraController>().SetLock(status);
        }

        private void SetCursorStatus(bool status)
        {
            if (status)
                Cursor.lockState = CursorLockMode.None;
            else
                Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = status;
        }
    }
}
