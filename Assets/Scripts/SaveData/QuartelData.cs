﻿using System;
using System.Collections.Generic;

namespace SaveData
{
    [Serializable]
    public class QuartelData
    {
        public int money;
        //public List<VehicleData> vehicles = new List<VehicleData>();
        public List<GarageData> Garages = new List<GarageData>();
    }
}