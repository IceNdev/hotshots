﻿using System;
using UnityEngine;

namespace SaveData
{
    [Serializable]
    public class PlayerData
    {
        public Vector3 position;
        public Quaternion rotation;
    }
}