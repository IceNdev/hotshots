﻿using System;
using Systems.Quartel;
using NPC;
using UnityEngine;

namespace SaveData.SaveDataManagers
{
    public class FirefighterDataManager : MonoBehaviour
    {
        [SerializeField] private GameObject[] _firefighterPrefabs;
        
        public void OnSave()
        {
            SaveData.current.Reset();
            int firefighters = QuartelManager.Instance.GetFirefighters().Count;
            
            foreach (Firefighter firefighter in QuartelManager.Instance.GetFirefighters())
            {
                FirefighterData firefighterData = new FirefighterData();
                firefighterData.isDriver = firefighter.IsDriver;

                SaveData.current.FireFighterData.Add(firefighterData);
            }
            
            // TODO: check other things
            if (firefighters > 0)
                SerializationManager.Save("firefighterInfo", SaveData.current);
            else
                SerializationManager.DeleteSave("firefighterInfo");
        }

        public void OnLoad()
        {
            SaveData.current.Reset();
            SaveData.current = (SaveData) SerializationManager.Load(Application.persistentDataPath + "/saves/firefighterInfo.save");
            
            for (int i = 0; i < SaveData.current.FireFighterData.Count; i++)
            {
                FirefighterData currentFirefighter = SaveData.current.FireFighterData[i];
                SpawnBed spawnBed = QuartelManager.Instance.GetSpawnBeds().Find(x => !x.IsOccupied);

                if (spawnBed == null) return;
                GameObject firefighterObject = Instantiate(_firefighterPrefabs[Convert.ToByte(currentFirefighter.isDriver)], spawnBed.GetPosition(), spawnBed.GetRotation());
                Firefighter firefighter = firefighterObject.GetComponent<Firefighter>();
                QuartelManager.Instance.UpdateSpawnBed(firefighter, spawnBed.GetPosition());
            }
        }
    }
}
