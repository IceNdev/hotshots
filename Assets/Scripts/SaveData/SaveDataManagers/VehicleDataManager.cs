﻿using System;
using System.Collections.Generic;
using Systems.Quartel;
using UnityEngine;
using UnityEngine.Assertions.Must;
using Utils;
using Vehicle;

namespace SaveData.SaveDataManagers
{
    public class VehicleDataManager : Singleton<VehicleDataManager>
    {
        [SerializeField] private GameObject[] _vehiclePrefabs;

        public void OnSave()
        {
            SaveData.current.Reset();
            int garages = QuartelManager.Instance.GetGarages().Count;
            
            foreach (Garage garage in QuartelManager.Instance.GetGarages())
            {
                VehicleInfo vehicle = garage.GetVehicle();
                VehicleData vehicleData = new VehicleData();
                vehicleData.position = vehicle.transform.position;
                vehicleData.rotation = vehicle.transform.rotation;
                vehicleData.vehicleType = vehicle.PrefabType;
                
                vehicleData.WaterTankData = new WaterTankData();
                vehicleData.WaterTankData.isFixed = false;
                vehicleData.WaterTankData.waterLevel = vehicle.GetComponent<WaterTank>().WaterLoaded;
                
                GarageData garageData = new GarageData();
                garageData.Vehicle = vehicleData;
                garageData.Position = garage.GetPosition();
                
                Debug.Log($"Name: {vehicle.name} | Position: {vehicle.transform.position}");
                SaveData.current.QuartelData.Garages.Add(garageData);
                SaveData.current.QuartelData.money = (int) EconomyManager.Instance.Money;
            }
            
            // TODO: check other things
            if(garages > 0)
                SerializationManager.Save("vehicleInfo", SaveData.current);
            else
                SerializationManager.DeleteSave("vehicleInfo");
        }

        public void OnLoad()
        {
            SaveData.current.Reset();
            SaveData.current = (SaveData) SerializationManager.Load(Application.persistentDataPath + "/saves/vehicleInfo.save");
            
            for (int i = 0; i < SaveData.current.QuartelData.Garages.Count; i++)
            {
                GarageData currentGarage = SaveData.current.QuartelData.Garages[i];
                VehicleData currentVehicle = currentGarage.Vehicle;
                GameObject vehicle = Instantiate(_vehiclePrefabs[(int)currentVehicle.vehicleType], currentVehicle.position, currentVehicle.rotation);
                vehicle.GetComponent<WaterTank>().SetWater(currentVehicle.WaterTankData.waterLevel);
                QuartelManager.Instance.UpdateGarage(vehicle.GetComponent<VehicleInfo>(), currentGarage.Position);
            }

            EconomyManager.Instance.Money = SaveData.current.QuartelData.money;
        }
    }
}
