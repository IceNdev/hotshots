﻿using System;
using UnityEngine;

namespace SaveData
{
    [Serializable]
    public class WaterTankData
    {
        public bool isFixed;
        public float waterLevel;
        public Vector3 position;
        public Quaternion rotation;
        //public GameObject prefab;
    }
}