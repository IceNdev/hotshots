﻿using System;
using UnityEngine;

namespace SaveData
{
    [Serializable]
    public class GarageData
    {
        public VehicleData Vehicle;
        public Vector3 Position;
    }
}