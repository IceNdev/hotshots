﻿using System;
using System.Collections.Generic;

namespace SaveData
{
    [Serializable]
    public class SaveData
    {
        private static SaveData _current;

        public SaveData()
        {
            PlayerData      = new PlayerData();
            QuartelData     = new QuartelData();
            HotbarData      = new HotbarData();
            FireFighterData = new List<FirefighterData>();
        }
        
        public static SaveData current
        {
            get
            {
                if (_current == null)
                    _current = new SaveData();
                
                return _current;
            }
            set
            {
                if(value != null)
                {
                    _current = value;
                }
            }
        }

        public PlayerData PlayerData;
        public QuartelData QuartelData = new QuartelData();
        public HotbarData HotbarData;
        public List<FirefighterData> FireFighterData;

        public void Reset()
        {
            _current        = null;
            PlayerData      = new PlayerData();
            QuartelData     = new QuartelData();
            HotbarData      = new HotbarData();
            FireFighterData = new List<FirefighterData>();
        }
    }
}