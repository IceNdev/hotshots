﻿using System;
using UnityEngine;

namespace SaveData
{
    [Serializable]
    public enum EPrefabType
    {
        VLCI,
        VUCI,
        VALE
    }
    
    [Serializable]
    public class VehicleData
    {
        public Vector3 position;
        public Quaternion rotation;
        public WaterTankData WaterTankData;
        public EPrefabType vehicleType;
    }
}